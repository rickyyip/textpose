module.exports = {


  friendlyName: 'Reset admin',


  description: '',


  fn: async function () {

    await User.updateOne({
      where: {
        emailAddress: 'admin@example.com'
      }
    })
    .set({
      isSuperAdmin: true,
      password: await sails.helpers.passwords.hashPassword('Text@dmin2020'),
    });

  }


};

