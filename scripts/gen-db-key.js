module.exports = {


  friendlyName: 'Generate DB encryption key',


  description: '',


  fn: async function () {
    const { v4: uuidv4 } = require('uuid');
    const {Base64} = require('js-base64');

    sails.log('New encryption key: ' + Base64.encode(uuidv4()));

  }


};

