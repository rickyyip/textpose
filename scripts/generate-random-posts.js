module.exports = {


  friendlyName: 'Generate random posts',


  description: 'Generate 50 posts that expire in 1 hour',


  fn: async function () {
    const { v4: uuidv4 } = require('uuid');
    const sha1 = require('sha1');

    for (let i=0; i < 50; i++){
      let sourceString = uuidv4() + new Date().getTime() + uuidv4() + sails.config.custom.hashSalt;
      var randomString = sha1( sourceString );
      var shortlink = randomString.substring(0,8);

      // eslint-disable-next-line no-undef
      await Post.create({
        title: 'Generated '+uuidv4(),
        content: uuidv4(),
        mode: 'text',
        expiry: 3600,
        expireAt: new Date().getTime() + 3600*1000,
        shortlink: shortlink,
        ip: '127.0.0.1'
      })
      .intercept( (err) => {
        console.log(err.message);
      });
    }

  }


};

