/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  '*': 'is-logged-in',

  'entrance/signup': 'is-super-admin',
  'entrance/view-signup': 'is-super-admin',

  'dashboard/*': 'is-super-admin',
  'post/*': 'is-super-admin',

  // Public access
  'entrance/*': true,
  'account/logout': true,
  'view-homepage-or-redirect': true,
  'view-faq': true,
  'view-contact': true,
  'view-about': true,
  'view-credits': true,
  'view-roadmap': true,
  'legal/view-terms': true,
  'legal/view-privacy': true,
  // 'deliver-contact-form-message': true,

  // Post - Public
  'post/view-create-post': true,
  'post/view-read-post': true,
  'post/view-read-post-raw': true,

  // Live post
  'live-post/view-create-live-post': true,
  'live-post/view-edit-live-post': true,
  'live-post/view-live-post': true,
  'live-post/save-live-post': true,

  // API - Public
  'get-captcha': true,
  'post/create-post': true,

  'live-post/create-live-post': true,
  'live-post/hello-live-post': true,
  'live-post/reconnect-live-post': true,
  'live-post/edit-live-post': true,
  'live-post/edit-live-post-title': true,
  'live-post/edit-live-post-mode': true,
  'live-post/get-changes': true,
  'live-post/update-live-post-content': true,
  'live-post/disconnect-editor': true,
  'live-post/disconnect-guest': true,
  'live-post/get-room-editor-count': true,

  'chat/get-messages': true,
  'chat/post-message': true,

};
