module.exports.cron = {

  checkPostExpiry: {
    schedule: '0 0/5 * * * *',
    onTick: async function() {
      // console.log('Tick!');
      await sails.helpers.checkAllPostsExpiry()
      .intercept( (err) => {
        return err.message;
      });

      await sails.helpers.checkAllLivePostsExpiry()
      .intercept( (err) => {
        return err.message;
      });

      await sails.helpers.checkAllLivePostsSave()
      .intercept( (err) => {
        return err.message;
      });
    }
  }

};
