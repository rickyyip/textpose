// For local development

module.exports = {

  datastores: {
    default: {
      adapter: 'sails-mongo',
      url: 'mongodb://localhost/textpose',
    },
  },

  session: {
    adapter: 'connect-mongo',
    url: 'mongodb://localhost/textpose',
    secret: 'f57c41d50a6e6b2f555fd240e731f991',
  },

  log: {
    level: 'debug'
  },

  custom: {
    baseUrl: 'http://localhost:1337',
    // internalEmailAddress: 'support@textpose.me',
    verifyEmailAddresses: false,
    
    redisDataUrl: 'redis://user:password@localhost:6379/databasenumber',

    // API Keys

    // mailgunApiKey: '',
    // stripePublishableKey: '',
    // stripeSecret: '',

    // sendgridSecret: 'SG.fake.3e0Bn0qSQVnwb1E4qNPz9JZP5vLZYqjh7sn8S93oSHU',
    // stripeSecret: 'sk_prod__fake_Nfgh82401348jaDa3lkZ0d9Hm',

  },



};
