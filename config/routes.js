/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  // POSTS
  // 'GET /':                   { action: 'view-homepage-or-redirect', locals: {title: 'New post', layout: 'layouts/layout-editor'} },
  'GET /:unused?':              { action: 'post/view-create-post', locals: {layout: 'layouts/layout-editor'} },
  // 'GET /new':                { action: 'post/view-create-post', locals: {title: 'New post', layout: 'layouts/layout-editor'} },
  'GET /p/:id?':                { action: 'post/view-read-post', locals: {layout: 'layouts/layout-editor'} },
  'GET /r/:id?':                { action: 'post/view-read-post-raw', locals: {layout: 'layouts/layout-raw'} },
  'GET /live':                  { action: 'live-post/view-create-live-post', locals: {title: 'New Live Post', layout: 'layouts/layout-dark'} },
  'GET /l/:id?':                { action: 'live-post/view-live-post', locals: { layout: 'layouts/layout-editor'} },
  'GET /le/:id?':               { action: 'live-post/view-edit-live-post', locals: { layout: 'layouts/layout-editor'} },

  // Dashboard
  'GET /dashboard':                      { action: 'dashboard/view-dashboard' },
  'GET /dashboard/posts/:page?':         { action: 'dashboard/view-post-list' },
  'GET /dashboard/live-posts/:page?':    { action: 'dashboard/view-live-post-list' },
  'GET /dashboard/edit/:id?':            { action: 'dashboard/view-edit-post', locals: {layout: 'layouts/layout-editor' }},

  'GET /faq':                { action:   'view-faq', locals: {title: 'FAQ'} },
  'GET /legal/terms':        { action:   'legal/view-terms', locals: {title: 'Terms'} },
  'GET /legal/privacy':      { action:   'legal/view-privacy', locals: {title: 'Privacy'} },
  // 'GET /contact':            { action:   'view-contact' },
  'GET /contact':            { action:   'view-faq' },

  'GET /signup':             { action: 'entrance/view-signup', locals: {title: 'Sing up'} },
  'GET /email/confirm':      { action: 'entrance/confirm-email' },
  'GET /email/confirmed':    { action: 'entrance/view-confirmed-email' },

  'GET /login':              { action: 'entrance/view-login', locals: {title: 'Login'} },
  'GET /password/forgot':    { action: 'entrance/view-forgot-password', locals: {title: 'Forgot password'} },
  'GET /password/new':       { action: 'entrance/view-new-password', locals: {title: 'Set password'} },

  'GET /account':            { action: 'account/view-account-overview', locals: {title: 'Account'} },
  'GET /account/password':   { action: 'account/view-edit-password', locals: {title: 'Password'} },
  'GET /account/profile':    { action: 'account/view-edit-profile', locals: {title: 'Profile'} },

  'GET /about':              { action: 'view-about', locals: {title: 'About'} },
  'GET /roadmap':            { action: 'view-roadmap', locals: {title: 'Roadmap'} },






  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗   ┬   ╔╦╗╔═╗╦ ╦╔╗╔╦  ╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗  ┌┼─   ║║║ ║║║║║║║║  ║ ║╠═╣ ║║╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝  └┘   ═╩╝╚═╝╚╩╝╝╚╝╩═╝╚═╝╩ ╩═╩╝╚═╝
  '/terms':                   '/legal/terms',
  '/logout':                  '/api/v1/account/logout',
  '/get-captcha':             '/api/v1/get-captcha',


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝
  // …


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
  '/api/v1/account/logout':                           { action: 'account/logout' },
  'PUT   /api/v1/account/update-password':            { action: 'account/update-password' },
  'PUT   /api/v1/account/update-profile':             { action: 'account/update-profile' },
  'PUT   /api/v1/account/update-billing-card':        { action: 'account/update-billing-card' },
  'PUT   /api/v1/entrance/login':                        { action: 'entrance/login' },
  'POST  /api/v1/entrance/signup':                       { action: 'entrance/signup' },
  'POST  /api/v1/entrance/send-password-recovery-email': { action: 'entrance/send-password-recovery-email' },
  'POST  /api/v1/entrance/update-password-and-login':    { action: 'entrance/update-password-and-login' },
  'POST  /api/v1/deliver-contact-form-message':          { action: 'deliver-contact-form-message' },

  'GET /api/v1/get-captcha':                                 { action: 'get-captcha' },

  // POSTS
  'POST    /api/v1/post/create-post':                        { action: 'post/create-post' },
  'PATCH   /api/v1/post/update-post':                        { action: 'post/update-post' },
  'DELETE  /api/v1/post/delete-post':                        { action: 'post/delete-post' },

  // Live posts
  'GET     /api/v1/live-post/get-room-editor-count/:id?':    { action: 'live-post/get-room-editor-count' },
  'GET     /api/v1/live-post/get-changes/:id?':              { action: 'live-post/get-changes' },
  'POST    /api/v1/live-post/create-live-post':              { action: 'live-post/create-live-post' },
  'POST    /api/v1/live-post/update-live-post-content/:id?': { action: 'live-post/update-live-post-content' },
  'POST    /api/v1/live-post/disconnect-guest/:id?':              { action: 'live-post/disconnect-guest' },


  // Live post requiring editorlink
  'POST    /api/v1/live-post/hello-live-post/:id?':               { action: 'live-post/hello-live-post' },
  'POST    /api/v1/live-post/reconnect-live-post/:id?':           { action: 'live-post/reconnect-live-post' },
  'POST    /api/v1/live-post/disconnect-editor/:id?':             { action: 'live-post/disconnect-editor' },
  'POST    /api/v1/live-post/save-live-post/:id?':                { action: 'live-post/save-live-post' },
  'PUT     /api/v1/live-post/edit-live-post/:id?':                { action: 'live-post/edit-live-post' },
  'PUT     /api/v1/live-post/edit-live-post-title/:id?':          { action: 'live-post/edit-live-post-title' },
  'PUT     /api/v1/live-post/edit-live-post-mode/:id?':           { action: 'live-post/edit-live-post-mode' },

  // Chat
  'GET     /api/v1/chat/get-messages/:room?':                     { action: 'chat/get-messages' },
  'POST    /api/v1/chat/post-message/:room?':                     { action: 'chat/post-message' },

};
