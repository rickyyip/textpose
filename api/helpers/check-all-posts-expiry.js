module.exports = {


  friendlyName: 'Check all posts expiry',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function () {
    const now = new Date().getTime();

    // eslint-disable-next-line no-undef
    await Post.destroy({
      where: {
        expireAt: {
          '<=': now
        }
      }
    });

    return {};
  }


};

