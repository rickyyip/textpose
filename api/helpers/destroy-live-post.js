module.exports = {


  friendlyName: 'Destroy live post',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({id}) {

    // Remove from main DB
    // eslint-disable-next-line no-undef
    let post = await LivePost.destroyOne({id: id});

    // Remove from redis: changes, step counter, sessions of guests and editors
    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });
    const delAsync = promisify(client.del).bind(client);
    const llenAsync = promisify(client.llen).bind(client);
    const ltrimAsync = promisify(client.ltrim).bind(client);

    const changesList = post.editorlink;
    const counterSet = post.editorlink+'-step';
    const editorSessionsSet = post.id+'-editors';
    const guestSessionsSet = post.id+'-guests';

    // Remove sessions
    await delAsync(editorSessionsSet, guestSessionsSet);

    // Delete counter
    await delAsync(counterSet);

    // Progressively empty changesList
    while (await llenAsync(changesList)){
      await ltrimAsync(changesList, 0, 100);
    }


    // TODO
    return {};
  }


};

