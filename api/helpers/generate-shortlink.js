module.exports = {


  friendlyName: 'Generate shortlink',


  description: 'Generate a shortlink that is unique to the DB.',


  inputs: {

    linkLength: {
      type: 'number',
      description: 'The length of shortlink',
      defaultsTo: 8,
      max: 40,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({linkLength}) {
    const sha1 = require('sha1');
    const { v4: uuidv4 } = require('uuid');

    // if (!linkLength){
    //   linkLength = 8;
    // }

    let shortlink = '';

    let fail = true;
    while (fail){
      let sourceString = uuidv4() + '-' + uuidv4() + '-' + sails.config.custom.hashSalt;
      let hash = sha1( sourceString );
      shortlink = hash.substring(0,linkLength);

      // eslint-disable-next-line no-undef
      let count = await Post.count({shortlink: shortlink});

      if (!count){ fail = false; }

    }

    return shortlink;
  }


};

