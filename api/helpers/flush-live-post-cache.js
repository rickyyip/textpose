module.exports = {


  friendlyName: 'Destroy live post',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    },

    editorlink: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({id, editorlink}) {
    // Remove from redis: changes, step counter, sessions of guests and editors
    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const delAsync = promisify(client.del).bind(client);

    const changesList = editorlink;
    const counterSet = editorlink+'-step';
    const editorSessionsSet = id+'-editors';
    const guestSessionsSet = id+'-guests';

    // Remove sessions
    await delAsync(editorSessionsSet, guestSessionsSet);

    // Delete counter
    await delAsync(counterSet);

    // Delete changes list
    await delAsync(changesList);


    // TODO
    return {};
  }


};

