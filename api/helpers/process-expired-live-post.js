module.exports = {


  friendlyName: 'Process expired live post',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    notFound: {
      description: 'Post not found.'
    },

  },


  fn: async function ({id}) {
    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({id: id});

    if (!post){ throw 'notFound'; }

    // Destry post if expiry after save is 0
    if (post.expiryAfterSave === 0){
      // eslint-disable-next-line no-undef
      await LivePost.destroyOne({id: id});

    } else {
      // Pull all changes and set as readonly
      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const getAsync = promisify(client.get).bind(client);
      const lrangeAsync = promisify(client.lrange).bind(client);
      // const setAsync = promisify(client.set).bind(client);

      // Get current step
      let globalStep = 0;
      let globalStepString = await getAsync(post.editorlink+'-step');
      if (globalStepString){
        globalStep = parseInt(globalStepString);
      }

      // let content = post.content;
      let contentStep = post.step;

      let changes = [];

      // Get all changes
      if (globalStep && contentStep > globalStep){
        changes = await lrangeAsync(post.editorlink, 0, -1);
      }

      console.log('While processing the expired live post, got changes:', changes);

      console.log('Actual:', await lrangeAsync(post.editorlink, 0, -1));

      // eslint-disable-next-line no-undef
      await LivePost.updateOne({id: id})
      .set({
        changes: changes,
        readonly: true,
        globalStep: globalStep,
      });

    }

    await sails.helpers.flushLivePostCache(post.id, post.editorlink);


    // let shortlink = await sails.helpers.generateShortlink();

    // // eslint-disable-next-line no-undef
    // await Post.create({
    //   title: post.title,
    //   content: post.content,
    //   expiry: post.expiryAfterSave,
    //   expireAt: _.now() + post.expiryAfterSave*1000,
    //   shortlink: shortlink,
    //   mode: post.mode,
    //   ip: post.ip,
    // })
    // .intercept( (err) => {
    //   console.log(err);
    //   return err.message;
    // });

    // TODO
    return {
      // shortlink: shortlink
    };
  }


};

