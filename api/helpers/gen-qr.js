module.exports = {


  friendlyName: 'Gen qr',


  description: '',


  inputs: {

    data: {
      type: 'ref',
      required: true
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    var qrcode = require('qrcode');

    var res = await qrcode.toString(inputs.data, {type: 'svg'}, (err, url) => {
      if (err){
        console.log(err);
        return err;
      } else {
        return url;
      }
    });

    return res;

  }


};

