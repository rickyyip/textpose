module.exports = {


  friendlyName: 'Get random name',


  description: '',


  inputs: {

    ignoreList: {
      type: 'ref',
      defaultsTo: []
    }

  },


  exits: {

    success: {
      outputFriendlyName: 'Random name',
    },

  },


  fn: async function (inputs) {

    var ignoreList = inputs.ignoreList;
    // if ( inputs.ignoreList.isArray() ){
    //   ignoreList = inputs.ignoreList;
    // }

    const nameList = [
      'Bugs Bunny',
      'SpongeBob',
      'Scooby Doo',
      'Daffy Duck',
      'Tom Cat',
      'Mickey Mouse',
      'Jerry Mouse',
      'Snoopy',
      'Goofy',
      'Winnie-the-pooh',
      'Homer Simpson',
      'Wile E. Coyote',
      'Shaggy Rogers',
      'Donald Duck',
      'Charlie Brown',
      'Fred Flintstone',
      'Stewie Griffin',
      'Bart Simpson',
      'Sylvester',
      'Taz',
      'Batman',
      'Shrek',
      'Eeyore',
      'Tigger',
      'Popeye',
      'Pink Panther',
      'Spider-Man',
      'Yosemite Sam',
      'Woody',
      'Porky Pig',
      'Marvin the Martian',
      'Yogi Bear',
      'Peter Griffin',
      'Patrick Star',
      'Elmer Fudd',
      'Bender',
      'Foghorn Leghorn',
      'Scrooge McDuck',
      'Tweety',
      'Woody Woodpecker',
      'Pepe Le Pew',
      'Ninja Turtle',
      'Brian Griffin',
      'Dory',
      'Speedy Gonzales',
      'Dexter',
      'Eric Cartman',
      'Chip n Dale',
      'The Powerpuff Girls',
      'Squidward Tentacles',
      'Lisa Simpson',
      'Barney Rubble',
      'Maggie Simpson',
      'Pikachu',
      'Courage',
      'Buzz Lightyear',
      'Garfield',
      'Joker',
      'Plankton',
      'Velma Dinkley',
      'Yakko',
      'Wakko',
      'Dot',
      'Pinky',
      'Marge Simpson',
      'Tommy Pickles',
      'Rick Sanchez',
      'Clifford',
      'Little Mermaid',
      'Linus van Pelt',
      'Fry',
      'Piglet',
      'Stitch',
      'Slinky Dog',
      'The Brain',
      'Goku',
      'Papa Smurf',
      'Pluto',
      'Olaf',
      'Optimus Prime',
      'Road Runner',
      'Rex',
      'Betty Boop',
      'Kermit the Frog',
      'George Jetson',
      'Superman',
      'Roger',
      'Skeletor',
      'Droopy Dog',
      'Perry the Platypus',
      'Puss in Boots',
      'Bullwinkle J. Moose',
      'Timmy Turner',
      'Bullseye',
      'Grumpy',
      'Kenny McCormick',
      'Mr Potato Head',
      'Cosmo',
      'Wanda',
      'Sebastian',
      'Minnie Mouse',
      'Deadpool',
      'Mr Burns',
      'Glenn Quagmire',
      'Blue',
      'Alvin Seville',
      'Felix the Cat',
      'Dopey',
      'Lumiere',
      'Flounder',
      'Finn the Human',
      'Frosty the Snowman',
      'Butters Stotch',
      'Inspecter Gadget',
      'Hamm',
      'Cinderella',
      'Bambi',
      'Grinch',
      'Baymax',
      'Gray',
      'Robin',
      'Hank Hill',
      'Eugene H. Krabs',
      'Mighty Mouse',
      'Snow White',
      'Iron Man',
      'Lola Bunny',
      'Huckleberry Hound',
      'Lucy van Pelt',
      'Sleepy',
      'Bashful',
      'Happy',
      'Sandy Cheeks',
      'Jake the Dog',
      'Captain Hook',
      'Morty Smith',
      'Fred Jones',
      'Lois Griffin',
      'Darwing Duck',
      'Underdog',
      'Minion',
      'Stan Smith',
      'Jimmy Neutron',
      'Butt-Head'
    ];

    var chosen = false;
    var name = '';
    var round = 0;

    while (!chosen && round < 50){
      let index = Math.floor( Math.random() * Math.floor(nameList.length) );
      let appandNumber = Math.floor( Math.random() * Math.floor(9)) + 1;

      name = nameList[index] + ' ' + appandNumber.toString();

      if (!ignoreList.includes(name)){ chosen = true; }
      round += 1;
    }

    if (!chosen){
      return new Error('Timeout.');
    }

    // Send back the result through the success exit.
    return name;

  }


};

