module.exports = {


  friendlyName: 'Create captcha',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function () {
    var svgCaptcha = require('svg-captcha');

    return svgCaptcha.create({
      size: 4,
      noise: 2,
      charPreset: 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ1234567890',
    });
  }


};

