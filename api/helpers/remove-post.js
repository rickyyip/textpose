module.exports = {


  friendlyName: 'Remove post',


  description: '',


  inputs: {

    shortlink: {
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({shortlink}) {
    // eslint-disable-next-line no-undef
    var post = await Post.destroyOne({ shortlink: shortlink});

    return post;
  }


};

