module.exports = {


  friendlyName: 'Save live post',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    notFound: {
      description: 'Post not found.',
    },

  },


  fn: async function ({id}) {

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({id: id}).populate('chat');

    // if (!post || post.editorlink !== post.editorlink){ throw 'notFound'; }

    if (post.expirySave > 0){

      // Pull all changes and set as readonly
      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const getAsync = promisify(client.get).bind(client);
      const lrangeAsync = promisify(client.lrange).bind(client);
      const llenAsync = promisify(client.llen).bind(client);
      // const setAsync = promisify(client.set).bind(client);

      // Get current step
      let globalStep = 0;
      let globalStepString = await getAsync(post.editorlink+'-step');
      if (globalStepString){
        globalStep = parseInt(globalStepString);
      }

      // let content = post.content;
      // let contentStep = post.step;

      // let changes = await lrangeAsync(post.editorlink, 0, -1);

      // Get all changes
      // if (globalStep && contentStep > globalStep){
      //   changes = await lrangeAsync(post.editorlink, 0, -1);
      // }

      // console.log('While saving the live post, got changes:', changes);

      // console.log('Actual:', await lrangeAsync(post.editorlink, 0, -1));

      // eslint-disable-next-line no-undef
      await LivePost.updateOne({id: id})
      .set({
        changes: await lrangeAsync(post.editorlink, 0, -1),
        readonly: true,
        globalStep: globalStep,
      });

      let messages = await lrangeAsync(post.id+'-chat', 0, -1);
      // console.log('Saving messages:', messages);
      // console.log('Live post chat:', post.chat);

      // Save chat history
      // eslint-disable-next-line no-undef
      await Chat.updateOne({
        where: {
          id: post.chat[0].id,
        },
      })
      .set({
        messages: messages,
        count: await llenAsync(id+'-chat'),
      });

    }

    // Flush everything in redis
    await sails.helpers.flushLivePostCache(post.id, post.editorlink);

    await sails.sockets.broadcast(post.id, 'savedPost');

    return {
      // shortlink: shortlink
    };
  }


};

