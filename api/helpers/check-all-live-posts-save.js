module.exports = {


  friendlyName: 'Check all live posts save',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    const now = new Date().getTime();

    // eslint-disable-next-line no-undef
    let posts = await LivePost.find({
      where: {
        saveAt: {
          '<=': now
        },
        readonly: false,
      }
    });


    for (let i=0; i<posts.length; i++){
      console.log('Live post being saved:', posts[i].id);
      await sails.helpers.saveLivePost(posts[i].id)
      .intercept( (err) => {
        console.log(err.message);
      });
    }

    return {};
  }


};

