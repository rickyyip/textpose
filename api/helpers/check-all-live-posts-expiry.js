module.exports = {


  friendlyName: 'Check all live posts expiry',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    const now = new Date().getTime();

    // eslint-disable-next-line no-undef
    let posts = await LivePost.find({
      where: {
        expireAt: {
          '<=': now
        }
      }
    });


    for (let i=0; i<posts.length; i++){
      console.log('Live post expired:', posts[i].id);
      await sails.helpers.destroyLivePost(posts[i].id);
    }

    return {};
  }


};

