/**
 * LivePost.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    title: {
      type: 'string',
      defaultsTo: '',
      maxLength: 300
    },

    content: {
      type: 'string',
      defaultsTo: ''
    },

    changes: {
      type: 'json',
      columnType: 'array',
      defaultsTo: [],
    },

    chatKey: {
      type: 'string',
      required: true,
      unique: true,
    },

    chat: {
      collection: 'chat',
      via: 'post',
    },

    readonly: {
      type: 'boolean',
      defaultsTo: false,
    },

    step: {
      type: 'number',
      defaultsTo: 0,
    },

    globalStep: {
      type: 'number',
      defaultsTo: 0,
    },

    expiryLive: {
      type: 'number',
      description: 'Expiry time in seconds for a live post, after which the post will be set to readonly.',
      defaultsTo: 10800,  // 3 hours
      isInteger: true,
      max: 2592000,  // 30 days
      min: 0,
    },

    expirySave: {
      type: 'number',
      description: 'The expiry time after a live posted is expired and set to readonly, after which the post will be removed.',
      defaultsTo: 3600,  // 1 hour
      max: 2592000,  // 30 days
      min: 0,
    },

    saveAt: {
      type: 'number',
      description: 'The epoch time to save a post and set to read only.',
      defaultsTo: 0,
    },

    expireAt: {
      type: 'number',
      description: 'The epoch time that the post expires',
      defaultsTo: 0
    },

    editorlink: {
      type: 'string',
      required: true,
      unique: true
    },

    guestlink: {
      type: 'string',
      unique: true
    },

    mode: {
      type: 'string',
      defaultsTo: 'txt'
    },

    ip: {
      type: 'string',
      isIP: true,
    },

    maxEditor: {
      type: 'number',
      description: 'The max number of people who can edit',
      defaultsTo: 10
    },

    maxGuest: {
      type: 'number',
      description: 'The max number of people who can view live',
      defaultsTo: 30
    },



    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

