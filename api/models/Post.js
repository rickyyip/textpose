/**
 * Post.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    title: {
      type: 'string',
      required: true,
      isNotEmptyString: true,
      maxLength: 300
    },

    content: {
      type: 'string',
      defaultsTo: ''
    },

    expiry: {
      type: 'number',
      description: 'Expiry time in seconds',
      required: true,
      isInteger: true,
      max: 2592000,
      min: 3600,
    },

    expireAt: {
      type: 'number',
      description: 'The epoch time that the post expires',
      defaultsTo: 0
    },

    shortlink: {
      type: 'string',
      required: true,
      unique: true
    },

    mode: {
      type: 'string',
      defaultsTo: 'txt'
    },

    ip: {
      type: 'string',
      isIP: true,
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

