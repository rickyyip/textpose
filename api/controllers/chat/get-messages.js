const { readFile } = require('fs');

module.exports = {


  friendlyName: 'Get messages',


  description: '',


  inputs: {

    from: {
      type: 'number',
      min: 0,
    },

    to: {
      type: 'number',
      min: 0,
    }

  },


  exits: {

    usageError: {
      responseType: 'usageError',
    },

    notFound: {
      responseType: 'notFound',
    }

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    // Define room as postID
    const room = this.req.param('room');
    // sails.sockets.join(this.req, room);

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: room,
      },
      select: ['id', 'readonly'],
    }).populate('chat');

    let messages = [];
    let chatCount = 0;
    let from = inputs.from;
    let to = inputs.to;

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const lrangeAsync = promisify(client.lrange).bind(client);
    const llenAsync = promisify(client.llen).bind(client);


    if (!post.readonly){
      chatCount = await llenAsync(room+'-chat');

      if (!chatCount){ throw 'notFound'; }

    } else {
      // if the post is readonly.

      messages = post.chat[0].messages;
      chatCount = post.chat[0].count;
      // console.log('Readonly message before slice:', messages);
    }

    // // Check bound
    // if ( (from && from > chatCount - 1) || (to && to > chatCount - 1) ){
    //   console.log('Error: requested range is out of bound.');
    //   throw 'usageError';
    // }

    // Limit a maximum of 25 messages at a time.
    // if neither "from" nor "to" is given, return the latest messages (up to 25 results)
    if (!from && !to){
      if (chatCount >= 25){
        from = chatCount - 24;
      } else {
        from = 0;
      }
      to = chatCount - 1;

      // if "from" is given but not "to", return up to 25 results from "from" (inclusive) up to the last entry at postition "chatCount - 1".
    } else if (from && !to){
      if (from + 24 < chatCount - 1){
        to = from + 24;
      } else {
        to = chatCount - 1;
      }

      // if "to" is given but not "from", return up to 25 results before "to" (inclusive) up to the first entry at position 0.
    } else if (!from && to){
      if (to - 24 >= 0){
        from = to - 24;
      } else {
        from = 0;
      }

      // if both "from" and "to" are given, check if "from" is <= "to", and the resulting range is not more than 25 items.
    } else{
      if (from > to || to - from > 24){
        console.log('Error: Range is invalid or requesting more than 25 messages. From:',from,'to',to);
        throw 'usageError';
      }
    }

    if (!post.readonly){
      messages = await lrangeAsync(room+'-chat', from, to);
    } else {
      // if the post is reaonly.
      messages = _.slice(messages, from, to + 1);
      // console.log('Readonly messages:', messages);
    }

    // All done. Return the specified range of messages that are stringified JSON objects.
    return {
      messages: messages,
      from: from,
      to: to,
      total: chatCount,
    };

  }


};
