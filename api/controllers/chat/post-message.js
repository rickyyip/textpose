module.exports = {


  friendlyName: 'Post message',


  description: '',


  inputs: {

    chatKey: {
      type: 'string',
      required: true,
      maxLength: 100,
    },

    type: {
      type: 'string',
      isIn: ['notice', 'message'],
      required: true,
    },

    from: {
      type: 'string',
      required: true,
      maxLength: 30,
    },

    body: {
      type: 'string',
      required: true,
      maxLength: 2000,
    }

  },


  exits: {

    badRequest: {
      responseType: 'badRequest',
    },

    notFound: {
      responseType: 'notFound',
    },

    serverError: {
      responseType: 'serverError'
    }

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    // Define room as postID
    const room = this.req.param('room');
    sails.sockets.join(this.req, room);

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const rpushAsync = promisify(client.rpush).bind(client);
    const getAsync = promisify(client.get).bind(client);
    const incrAsync = promisify(client.incr).bind(client);

    let chatKey = await getAsync(room+'-chatkey');

    if (chatKey !== inputs.chatKey){
      throw 'notFound';
    }

    let message = {
      id: await incrAsync(room+'-chatcount'),
      type: inputs.type,
      time: _.now(),
      from: inputs.from,
      body: inputs.body,
    };

    // Push message to redis
    let redisRes = undefined;
    try{
      redisRes = await rpushAsync(room+'-chat', JSON.stringify(message));
    }catch(err){
      console.log(err.message);
      throw 'serverError';
    }

    // console.log('PostID:', postID);
    // console.log('New message:', message);

    // Announce new user to the room
    sails.sockets.broadcast(room, 'newMessage', {
      id: message.id,
      type: message.type,
      time: message.time,
      from: message.from,
      body: message.body,
    }, this.req );

    // All done.
    return message.id;

  }


};
