module.exports = {


  friendlyName: 'View homepage or redirect',


  description: 'Display or redirect to the appropriate homepage, depending on login status.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/create-post'
    },

    redirect: {
      responseType: 'redirect',
    },

  },


  fn: async function () {

    if (this.req.me) {
      throw {redirect:'/dashboard'};
    }

    return {
      baseUrl: sails.config.custom.baseUrl
    };

  }


};
