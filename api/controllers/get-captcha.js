module.exports = {


  friendlyName: 'Get captcha',


  description: '',


  inputs: {

  },


  exits: {

  },


  fn: async function () {
    var captcha = await sails.helpers.createCaptcha();

    this.req.session.captcha = captcha.text;
    this.req.session.captchaTimeout = new Date().getTime() + 60 * 1000;

    // All done.
    return captcha.data;

  }


};
