module.exports = {


  friendlyName: 'View credits',


  description: 'Display "Credits" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/credits'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
