module.exports = {


  friendlyName: 'Reconnect live post',


  description: '',


  inputs: {
    editorlink: {
      type: 'string',
      defaultsTo: '',
    },

    guestlink: {
      type: 'string',
      defaultsTo: '',
    }

  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    usageError: {
      responseType: 'usageError',
    },

    notFound: {
      responseType: 'notFound',
    },
  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }


    const editorlink = inputs.editorlink;
    const guestlink = inputs.guestlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['editorlink', 'guestlink'],
    });

    if (!post){ throw 'notFound'; }
    if (post.readonly){ throw 'usageError'; }

    // let announce = '';

    if (editorlink){
      if (post.editorlink !== editorlink){ throw 'notFound'; }

      // announce = 'hello-editor';

    } else if (guestlink){
      if (post.guestlink !== guestlink){ throw 'notFound'; }

      // announce = 'hello-guest';
    } else {
      throw 'notFound';
    }

    // let name = this.req.cookies.chatname;
    // if (!name){
    //   name = '';
    // }

    // Join room
    sails.sockets.join(this.req, postID);

    console.log('Reconnecting:', this.req.sessionID,'to room:', postID);

    // Announce new user to the room
    // sails.sockets.broadcast(postID, announce, {
    //   name: name,
    // }, this.req );

    // All done.
    return 'hello again';

  }


};
