module.exports = {


  friendlyName: 'View edit live post',


  description: 'Display "Edit live post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/edit-live-post'
    },

    notFound: {
      responseType: 'notFound',
    },

    redirect: {
      responseType: 'redirect'
    }

  },


  fn: async function () {

    const editorlink = this.req.param('id');
    const sessionID = this.req.sessionID;

    // eslint-disable-next-line no-undef
    var post = await LivePost.findOne({
      where: {
        editorlink: editorlink
      },
      select: ['id', 'title', 'readonly', 'expiryLive', 'expirySave', 'expireAt', 'saveAt', 'editorlink', 'guestlink', 'mode', 'maxEditor', 'maxGuest', 'chatKey']
    });

    if (!post){
      throw 'notFound';
    }

    if (post.readonly){
      throw {redirect: '/l/'+post.guestlink };
    }

    // Inquire redis for current editor count and check for message count

    const roomSessionsSet = post.id+'-editors';

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const saddAsync = promisify(client.sadd).bind(client);
    const smembersAsync = promisify(client.smembers).bind(client);
    const sismemberAsync = promisify(client.sismember).bind(client);
    const llenAsync = promisify(client.llen).bind(client);
    const rpushAsync = promisify(client.rpush).bind(client);
    // const sremAsync = promisify(client.srem).bind(client);

    let editorCount = 0;
    let existInSession = false;

    // Check room session list length (editor count)
    let roomSessions = await smembersAsync(roomSessionsSet);
    editorCount = roomSessions.length;
    console.log('Total sessions:', roomSessions.length);
    // console.log(roomSessions);

    if (roomSessions){
      if (await sismemberAsync(roomSessionsSet, sessionID)){
        existInSession = true;
      } else if ( editorCount >= post.maxEditor){
        // User is not already in session lists. Check if there is a slot for the new editor. If not, throw redirect
        throw {redirect: '/l/'+post.guestlink+'?notify=room-was-full'};
      }
    }

    if (!roomSessions || !existInSession){
      // Add session to roomSessions
      await saddAsync(roomSessionsSet, sessionID);
      // console.log('Added session:', sessionID);
      editorCount += 1;

    } else {
      // console.log('Found session:', sessionID);
    }

    // Check if chat room exists. If it does not exist, either because it was not initialised or redis has been flushed recently, create a new one.
    let messageCount = await llenAsync(post.id+'-chat');
    if (!messageCount){
      let message = {
        type: 'notice',
        from: 'System',
        time: _.now(),
        body: 'Chat room has been created.',
      };

      await rpushAsync(post.id+'-chat', message);
    }


    // else{
    //   var date = new Date;
    //   var expiry = post.createdAt + post.expiry * 1000;
    //   if (expiry < date.getTime()){
    //     await sails.helper.removePost(post.id);
    //     throw 'notFound';
    //   }
    // }

    // Check if there is already a name set in the cookie
    let name = this.req.cookies.chatname;
    if (!name){
    // Give the user a new pseudo-name if none is set in cookie
      name = await sails.helpers.getRandomName();
      this.res.cookie('chatname', name);
    }

    var guestQrcode = await sails.helpers.genQr(sails.config.custom.baseUrl+'/l/'+post.guestlink);
    var editorQrcode = await sails.helpers.genQr(sails.config.custom.baseUrl+'/le/'+post.editorlink);

    // Respond with view.
    return {
      title: post.title,
      formData: post,
      editorlink: post.editorlink,
      guestlink: post.guestlink,
      baseUrl: sails.config.custom.baseUrl,
      guestQrcode: guestQrcode,
      editorQrcode: editorQrcode,
      editorCount: editorCount,
      messageCount: messageCount,
      chatname: name,
    };

  }


};
