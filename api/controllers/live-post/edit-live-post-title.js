module.exports = {


  friendlyName: 'Edit live post title',


  description: '',


  inputs: {

    title: {
      type: 'string',
      required: true
    },

    editorlink: {
      type: 'string',
      required: true,
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const editorlink = inputs.editorlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['editorlink'],
    });

    if (!post || post.editorlink !== editorlink){ throw 'notFound'; }

    // Join room
    sails.sockets.join(this.req, postID);
    // Announce new user to the room
    sails.sockets.broadcast(postID, 'updateTitle', {
      // step: step,
      title: inputs.title
    }, this.req );

    // eslint-disable-next-line no-undef
    await LivePost.updateOne({ id: postID})
    .set({
      title: inputs.title,
    });

    // All done.
    return;

  }


};
