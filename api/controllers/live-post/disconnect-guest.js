module.exports = {


  friendlyName: 'Disconnect socket',


  description: '',


  inputs: {
    guestlink: {
      type: 'string',
      required: true
    },
  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const guestlink = inputs.guestlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['id', 'guestlink'],
    });

    console.log('Disconnecting...');
    console.log('PostID:', postID);
    console.log('Post guestlink:', post.guestlink);
    console.log('Guestlink:', guestlink);

    if (!post || post.guestlink !== guestlink){ throw 'notFound'; }

    // Remove session from list
    try{
      const roomSessionsSet = post.id+'-guests';

      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const sismemberAsync = promisify(client.sismember).bind(client);
      const sremAsync = promisify(client.srem).bind(client);

      if (await sismemberAsync(roomSessionsSet, this.req.sessionID)){
        await sremAsync(roomSessionsSet, this.req.sessionID);

        // Broadcast to room
        sails.sockets.broadcast(postID, 'guestLeft', {}, this.req);
      }

    } catch(err){
      console.log(err.message);
    }


    // All done.
    return;

  }


};
