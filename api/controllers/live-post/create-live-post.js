module.exports = {


  friendlyName: 'Create live post',


  description: '',


  inputs: {

    title: {
      type: 'string',
      requried: true,
      maxLength: 300,
      isNotEmptyString: true,
    },

    expiryLive: {
      type: 'number',
      description: 'Expiry time in seconds',
      required: true,
      isInteger: true,
      max: 2592000,
      min: 0,
    },

    expirySave: {
      type: 'number',
      description: 'Expiry time in seconds',
      required: true,
      isInteger: true,
      max: 2592000,
      min: 0,
    },

    mode: {
      type: 'string',
      defaultsTo: 'txt',
      maxLength: 20
    },

    maxEditor: {
      type: 'number',
      defaultsTo: 10
    },

    maxGuest: {
      type: 'number',
      defaultsTo: 30
    },

  },


  exits: {

  },


  fn: async function (inputs) {
    // const sha1 = require('sha1');
    const { v4: uuidv4 } = require('uuid');
    let now = new Date().getTime();

    let postData = inputs;

    let saveAt = now + postData.expiryLive*1000;  // When to save post
    let expireAt = now + postData.expiryLive*1000+ postData.expirySave*1000;  // When to remove post
    let ip = this.req.ip;

    let editorlink = '';
    let guestlink = '';

    let post = {};

    // Create DB entry
    let created = false;
    while (!created){
      editorlink = await sails.helpers.generateLiveShortlink(8);
      guestlink = await sails.helpers.generateLiveShortlink(8);
      let chatKey = uuidv4();

      try{
        // eslint-disable-next-line no-undef
        post = await LivePost.create({
          title: postData.title,
          saveAt: saveAt,
          expireAt: expireAt,
          expiryLive: postData.expiryLive,
          expirySave: postData.expirySave,
          editorlink: editorlink,
          guestlink: guestlink,
          mode: postData.mode,
          ip: ip,
          maxEditor: postData.maxEditor,
          maxGuest: postData.maxGuest,
          chatKey: chatKey,
        })
        .intercept( (err) => {
          console.log(err);
          // return err.message;
        })
        .fetch();

        created = true;

      } catch(err) {
        console.log(err);
        continue;
      }
    }

    try{
      // Create redis entries
      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const setAsync = promisify(client.set).bind(client);
      const rpushAsync = promisify(client.rpush).bind(client);

      let initialMessage = {
        id: 0,
        type: 'notice',
        from: 'System',
        time: _.now(),
        body: 'Chat room has been created.',
      };

      await setAsync(post.id+'-editorlink', editorlink);
      await setAsync(editorlink+'-step', 0);
      await rpushAsync(post.id+'-chat', JSON.stringify(initialMessage));    // Chat history
      await setAsync(post.id+'-chatcount', 0);
      await setAsync(post.id+'-chatkey', post.chatKey);   // Chat key

    } catch(err){
      console.log(err.message);

      console.log('Error creating live post. Cleaning up...');
      // Remove post if redis failed
      // eslint-disable-next-line no-undef
      await LivePost.destroyOne({id: post.id})
      .intercept((err) => {
        console.log(err.message);
        return err.message;
      });
      return (err.message);
    }

    // Create chat room
    // eslint-disable-next-line no-undef
    let chat = await Chat.create().fetch();

    // eslint-disable-next-line no-undef
    await LivePost.replaceCollection(post.id, 'chat')
    .members(chat.id);

    // All done.
    return {
      editorlink: editorlink,
      guestlink: guestlink,
    };

  }


};
