module.exports = {


  friendlyName: 'View create live post',


  description: 'Display "Create live post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/create-live-post'
    }

  },


  fn: async function () {

    // Respond with view.
    return {
      baseUrl: sails.config.custom.baseUrl,
    };

  }


};
