module.exports = {


  friendlyName: 'Update live post content',


  description: '',


  inputs: {

    content: {
      type: 'string',
      maxLength: 512000,
    },

    step: {
      type: 'number',
      required: true,
    },

    editorlink: {
      type: 'string',
      required: true,
    },


  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    usageError: {
      responseType: 'usageError',
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const editorlink = inputs.editorlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['editorlink'],
    });

    if (!post || post.editorlink !== editorlink){ throw 'notFound'; }

    if (post.readonly){ throw 'usageError'; }


    // eslint-disable-next-line no-undef
    await LivePost.updateOne({id: postID})
    .set({
      content: inputs.content,
      step: inputs.step,
    });

    // Trim redis DB changes records

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    // const getAsync = promisify(client.get).bind(client);
    const lrangeAsync = promisify(client.lrange).bind(client);
    const ltrimAsync = promisify(client.ltrim).bind(client);

    let firstItem = await lrangeAsync(editorlink, 0, 0);
    let itemStep = 0;
    if (firstItem.length > 0){
      itemStep = JSON.parse(firstItem).step;
    }
    console.log('First item has step:', itemStep);
    console.log('Input has step:', inputs.step);
    let stepsToTrim = inputs.step - itemStep - 29;  // Give 30 steps of buffer

    // Trim DB if the first item is smaller than post step
    if (stepsToTrim > 0){
      console.log('Trimming',stepsToTrim,'records.');
      await ltrimAsync(editorlink, stepsToTrim, -1);
    }

    // All done.
    return;

  }


};
