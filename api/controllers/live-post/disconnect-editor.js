module.exports = {


  friendlyName: 'Disconnect socket',


  description: '',


  inputs: {
    editorlink: {
      type: 'string',
      required: true
    },

    name: {
      type: 'string',
      defaultsTo: 'Anonymous',
    }
  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    notFound: {
      responseType: 'notFound',
    },
  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const editorlink = inputs.editorlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['id', 'editorlink'],
    });

    if (!post || post.editorlink !== editorlink){ throw 'notFound'; }


    // Remove session from list
    try{
      const roomSessionsSet = post.id+'-editors';

      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const sismemberAsync = promisify(client.sismember).bind(client);
      const sremAsync = promisify(client.srem).bind(client);

      if (await sismemberAsync(roomSessionsSet, this.req.sessionID)){
        await sremAsync(roomSessionsSet, this.req.sessionID);

        // Broadcast to room
        sails.sockets.broadcast(postID, 'editorLeft', {
          name: inputs.name,
        }, this.req);
      }

    } catch(err){
      console.log(err.message);
    }


    // All done.
    return;

  }


};
