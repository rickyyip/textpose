module.exports = {


  friendlyName: 'Edit live post mode',


  description: '',


  inputs: {

    mode: {
      type: 'string',
      required: true
    },

    editorlink: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound',
    }

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const editorlink = inputs.editorlink;
    const postID = this.req.param('id');

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({
      where: {
        id: postID
      },
      select: ['editorlink'],
    });

    if (!post || post.editorlink !== editorlink){ throw 'notFound'; }

    // Join room
    // sails.sockets.join(this.req, editorlink);
    sails.sockets.join(this.req, postID);
    // Announce new user to the room
    sails.sockets.broadcast(postID, 'updateMode', {
      // step: step,
      mode: inputs.mode
    }, this.req );

    // eslint-disable-next-line no-undef
    await LivePost.updateOne({ id: postID})
    .set({
      mode: inputs.mode,
    });

    // All done.
    return;

  }


};
