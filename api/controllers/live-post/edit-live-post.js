module.exports = {


  friendlyName: 'Edit live post',


  description: '',


  inputs: {

    change: {
      type: 'json',
      required: true,
    },

    editorlink: {
      type: 'string',
      required: true,
    },

  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    serverError: {
      responseType: 'serverError',
      description: 'Global counter is out of sync! Please reload the document.'
    },

    usageError: {
      responseType: 'usageError',
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    // Check content length
    let docSize = JSON.stringify(inputs.change).length;

    if ( docSize > 512000){
      throw 'usageError';
    }

    const editorlink = inputs.editorlink;
    const postID = this.req.param('id');

    if (!postID){ throw 'badRequest'; }

    // Join room
    // sails.sockets.join(this.req, editorlink);
    await sails.sockets.join(this.req, postID);

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const incrAsync = promisify(client.incr).bind(client);
    const rpushAsync = promisify(client.rpush).bind(client);
    const lrangeAsync = promisify(client.lrange).bind(client);
    const setAsync = promisify(client.set).bind(client);
    const getAsync = promisify(client.get).bind(client);

    // Verify editorlink
    let editorlinkCache = await getAsync(postID+'-editorlink');

    // If there is no editorlink cached in redis, set one
    if (!editorlinkCache){
      // eslint-disable-next-line no-undef
      let post = await LivePost.findOne({id: postID});

      // If post is read-only, return usageError
      if (post.readonly){ throw 'usageError'; }

      await setAsync(postID+'-editorlink', post.editorlink );
    } else {
      if (editorlink !== editorlinkCache){ throw 'usageError'; }
    }

    let globalStep = await incrAsync(editorlink+'-step');
    console.log('Processing change:', globalStep);

    let change = {
      change: inputs.change,
      step: globalStep,
    };

    // If the step is 1, check whether this is actually the first ever change to the document in case the redis DB is purged during operation.
    if (globalStep <= 1){
      // eslint-disable-next-line no-undef
      let post = await LivePost.findOne({id: postID});
      if (!post){ throw 'notFound'; }

      if (post.step > 0){
        console.log('WARNING! Glogal counter is out of sync! Changes not saved to DB are most likely lost. Throwing an error...');
        console.log('Current global step is:', globalStep);
        await setAsync(editorlink+'-step', post.step);
        throw 'serverError';
      } else {
        // There is no global step nor post step. This is most likely a new post. Do nothing.
      }
    }

    // Operational transformation:
    // If two changes are submitted at the same time on the same line (i.e an user submit a change before a conflicting change could be pushed to the user), transform the position of this change.

    if (change.step > inputs.change.step && change.change.action === 'insert'){
      console.log('Conflicting change:', change);
      // Get all steps between the expected step and this actual step
      let changes = await lrangeAsync(editorlink, 0 - (change.step - inputs.change.step), -1);
      console.log('Changes:', changes);

      for (let i=0; i < changes.length; i++){
        console.log('Item:', i);
        // Check for conflict, where both starts on the same row
        let change2 = JSON.parse(changes[i]);

        console.log('Comparing with:', change2);

        if (change2.change.action === 'insert' ){

          if (change.change.start.row === change2.change.start.row && change.change.start.column >= change2.change.start.column){
            // Start on the same line
            let rowOffset = change2.change.end.row - change2.change.start.row;

            // Is the previous change multi-line?
            if (rowOffset === 0){
              // Plus the length of change2
              change.change.start.column += change2.change.end.column - change2.change.start.column;
            } else {
              // Plus the length of the last line of change2
              change.change.start.column += change2.change.end.column;
            }

            // Is this change multi-line? If not, plus the length of last line of change2.
            if (change.change.end.row - change.change.start.row === 0){
              change.change.end.column += change2.change.end.column - change2.change.start.column;
            }

            change.change.start.row += rowOffset;
            change.change.end.row += rowOffset;

            console.log('After transform:', change);

          } else if (change.change.start.row > change2.change.start.row && change2.change.end.row - change2.change.start.row > 0){
            // After change2, and change2 is multi-line
            let rowOffset = change2.change.end.row - change2.change.start.row;

            change.change.start.row += rowOffset;
            change.change.end.row += rowOffset;

            console.log('After transform:', change);
          }

        } else {
          console.log('change2 is "remove".');

          let enclosed = false;

          // If change start is within change2, move change to the beginning of change2
          if (change.change.start.row > change2.change.start.row && change.change.start.row < change2.change.end.row){
            enclosed = true;
          } else if (change.change.start.row === change2.change.start.row && change.change.start.column >= change2.change.start.column){
            enclosed = true;
          } else if (change.change.start.row === change2.change.end.row && change.change.start.column <= change2.change.end.column){
            enclosed = true;
          }

          if (enclosed){
            let rowOffset = change.change.start.row - change2.change.start.row;

            // Move the change start column to the change2 start column
            change.change.start.column = change2.change.start.column;

            // Is this change multi-line? If not, minus the length of last line of change2.
            if (change.change.end.row - change.change.start.row === 0){
              change.change.end.column -= change2.change.end.column - change2.change.start.column;
            }

            change.change.start.row -= rowOffset;
            change.change.end.row -= rowOffset;
            change.change.lines[0] = '';

            console.log('After transform:', change);
          }

          if (!enclosed){
            // If both start on the same line
            if (change.change.start.row === change.change.end.row && change.change.start.column >= change2.change.end.column){

              let rowOffset = change2.change.end.row - change2.change.start.row;

              // If the previous change is single-line
              if (rowOffset === 0){
                // Minus the length of change2
                change.change.start.column -= change2.change.end.column - change2.change.start.column;

                // Is this change multi-line? If not, minus the length of last line of change2.
                if (change.change.end.row - change.change.start.row === 0){
                  change.change.end.column -= change2.change.end.column - change2.change.start.column;
                }

              // If the previous change is multi-line
              } else {
                // Minus the length of the last line of change2
                change.change.start.column -= change2.change.end.column;

                // Is this change multi-line? If not, minus the length of last line of change2.
                if (change.change.end.row - change.change.start.row === 0){
                  change.change.end.column -= change2.change.end.column;
                }
              }

              change.change.start.row -= rowOffset;
              change.change.end.row -= rowOffset;

              console.log('After transform:', change);

            } else if (change.change.start.row > change2.change.end.row && change2.change.end.row - change2.change.start.row > 0){
              let rowOffset = change2.change.end.row - change2.change.start.row;
              change.change.start.row -= rowOffset;
              change.change.end.row -= rowOffset;

              console.log('After transform:', change);
            }
          }

        }

      } // End of for-loop

    }



    // Push the change to redis
    await rpushAsync(editorlink, JSON.stringify(change));

    // console.log('PostID:', postID);
    console.log('Pushed change:', globalStep);

    // Announce new user to the room
    await sails.sockets.broadcast(postID, 'updateBody', {
      step: globalStep,
      change: inputs.change
    }, this.req );

    // All done.
    return {
      globalStep: globalStep,
    };

  }


};
