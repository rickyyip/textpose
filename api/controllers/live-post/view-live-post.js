module.exports = {


  friendlyName: 'View live post',


  description: 'Display "Live post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/live-post'
    },

    notFound: {
      responseType: 'notFound',
    },

    serviceBusy: {
      statusCode: 503,
      viewTemplatePath: '503'
    }

  },


  fn: async function () {

    const guestlink = this.req.param('id');
    const sessionID = this.req.sessionID;

    // eslint-disable-next-line no-undef
    var post = await LivePost.findOne({
      where: {
        guestlink: guestlink
      },
      select: ['id', 'title', 'readonly', 'expiryLive', 'expirySave', 'expireAt', 'saveAt', 'guestlink', 'mode', 'maxGuest']
    });

    if (!post){
      throw 'notFound';
    }


    var guestQrcode = await sails.helpers.genQr(sails.config.custom.baseUrl+'/l/'+post.guestlink);

    if (post.readonly){
      return {
        formData: post,
        readonly: post.readonly,
        guestlink: post.guestlink,
        baseUrl: sails.config.custom.baseUrl,
        guestQrcode: guestQrcode,
      };
    }

    // Inquire redis for current editor count

    const roomSessionsSet = post.id+'-guests';

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const saddAsync = promisify(client.sadd).bind(client);
    const smembersAsync = promisify(client.smembers).bind(client);
    const sismemberAsync = promisify(client.sismember).bind(client);
    // const sremAsync = promisify(client.srem).bind(client);

    let guestCount = 0;
    let existInSession = false;

    // Check room session list length
    let roomSessions = await smembersAsync(roomSessionsSet);
    guestCount = roomSessions.length;
    console.log('Total sessions:', roomSessions.length);

    if (roomSessions){
      if (await sismemberAsync(roomSessionsSet, sessionID)){
        existInSession = true;
      } else if ( guestCount >= post.maxGuest){
        // Room has reached max capacity. If the session is not already in session list, throw an error
        // this.res.view('503');
        throw 'serviceBusy';
      }
    }

    if (!roomSessions || !existInSession){
      // Add session to roomSessions
      await saddAsync(roomSessionsSet, sessionID);
      console.log('Added session:', sessionID);
      guestCount += 1;

    } else {
      console.log('Found session:', sessionID);
    }


    // Respond with view.
    return {
      title: post.title,
      formData: post,
      guestlink: post.guestlink,
      baseUrl: sails.config.custom.baseUrl,
      guestQrcode: guestQrcode,
      guestCount: guestCount,
    };

  }


};
