module.exports = {


  friendlyName: 'Save live post',


  description: '',


  inputs: {

    editorlink: {
      type: 'string',
      required: true
    },

  },


  exits: {

    badRequest: {
      responseType: 'badRequest',
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const postID = this.req.param('id');
    const editorlink = inputs.editorlink;

    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({id: postID});

    if (!post || post.editorlink !== editorlink){ throw 'notFound'; }

    await sails.helpers.saveLivePost(postID)
    .intercept( (err) => {
      console.log(err.message);
      return err.message;
    });

    // All done.
    return;

  }


};
