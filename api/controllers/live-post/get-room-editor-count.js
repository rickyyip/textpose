module.exports = {


  friendlyName: 'Get room editor count',


  description: '',


  inputs: {

  },


  exits: {
    badRequest: {
      responseType: 'badRequest',
    },

    notFound: {
      responseType: 'notFound',
    }
  },


  fn: async function (inputs) {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const postID = this.req.param('id');

    console.log('postID:', postID);

    const roomSessionsSet = postID+'-editors';

    console.log('roomSessionsSet:', roomSessionsSet);

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const smembersAsync = promisify(client.smembers).bind(client);

    let editorCount = 0;
    // Check room session list length
    let roomSessions = await smembersAsync(roomSessionsSet);
    if (roomSessions){
      let editorCount = roomSessions.length;
      console.log('Got length:', editorCount);
    }

    // // eslint-disable-next-line no-undef
    // let post = await LivePost.findOne({
    //   where: {
    //     id: postID
    //   },
    //   select: ['editorlink'],
    // });

    // if (!post){ throw 'notFound'; }




    // All done.
    return {
      editorCount: editorCount,
    };

  }


};
