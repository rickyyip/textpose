module.exports = {


  friendlyName: 'Get changes',


  description: '',


  inputs: {

  },


  exits: {

    notFound: {
      responseType: 'notFound',
    },

    badRequest: {
      responseType: 'badRequest',
    },

  },


  fn: async function () {
    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const postID = this.req.param('id');

    // Join the connection to the post common room to receive events of post changes
    sails.sockets.join(this.req, postID);

    // Get post cache and step
    // eslint-disable-next-line no-undef
    let post = await LivePost.findOne({id: postID});

    if (!post){ throw 'notFound'; }

    const editorlink = post.editorlink;

    let content = post.content;
    let contentStep = post.step;
    let globalStep = 0;
    let changes = undefined;

    if (post.readonly){
      globalStep = post.globalStep;
      changes = post.changes;
    } else {

      const { promisify } = require('util');
      const redis = require('redis');
      const client = redis.createClient({
        url: sails.config.custom.redisDataUrl,
      });

      const getAsync = promisify(client.get).bind(client);
      const lrangeAsync = promisify(client.lrange).bind(client);
      const setAsync = promisify(client.set).bind(client);

      // Get current step

      let globalStepString = await getAsync(editorlink+'-step');
      if (globalStepString){
        globalStep = parseInt(globalStepString);
      }

      // Get all changes
      if (globalStep){

        changes = JSON.parse(await lrangeAsync(editorlink, 0, 0));

        console.log('First change in record:', changes);

        if (changes.step < post.step){
          changes = await lrangeAsync(editorlink, post.step - changes.step + 1, -1);
        } else if(changes.step > post.step){
          changes = await lrangeAsync(editorlink, 0, -1);
        }

        if (changes.length){
          console.log('Got changes. First change is step:', JSON.parse(changes[0]).step, 'Post step:', post.step);
        }

      } else if (post.step){
        // There is a post step but not a global step. The redis DB could be purged before all changes were saved.
        console.log('WARNING! Global step is not found, but content step exists. Redis DB could have been purged recently. Resuming the global counnter at the post step.');
        await setAsync(postID+'-step', post.step);
        globalStep = post.step;
      }

    }


    // console.log(changes);
    // if (content){
    //   console.log('Got contents at step:', post.step);
    // }
    // console.log(changes);
    // console.log('Global step at:', globalStep);

    // All done.
    return {
      title: post.title,
      mode: post.mode,
      content: content,
      contentStep: contentStep,
      globalStep: globalStep,
      changes: changes,
    };

  }


};
