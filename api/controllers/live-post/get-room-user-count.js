module.exports = {


  friendlyName: 'Get room user count',


  description: '',


  inputs: {

    guestlink: {
      type: 'string',
    },

    editorlink: {
      type: 'string',
    },

  },


  exits: {

    usageError: {
      responseType: 'usageError'
    }

  },


  fn: async function (inputs) {

    if (!inputs.guestlink && !inputs.editorlink){
      throw 'usageError';
    }

    let link = inputs.guest;

    if (!link){
      inputs.editorlink;
    }

    let post = await LivePost.findOne();

    const { promisify } = require('util');
    const redis = require('redis');
    const client = redis.createClient({
      url: sails.config.custom.redisDataUrl,
    });

    const smembersAsync = promisify(client.smembers).bind(client);

    // Check room session list length
    let roomSessions = await smembersAsync(roomSessionsSet);
    editorCount = roomSessions.length;

    // All done.
    return;

  }


};
