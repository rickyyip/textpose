module.exports = {


  friendlyName: 'View create post',


  description: 'Display "Create post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/create-post',
    }

  },


  fn: async function () {

    // Respond with view.
    return {
      baseUrl: sails.config.custom.baseUrl
    };

  }


};
