module.exports = {


  friendlyName: 'View read post raw',


  description: 'Display "Read post raw" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/read-post-raw'
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function () {
    // eslint-disable-next-line no-undef
    var post = await Post.findOne({
      where: {
        shortlink: this.req.param('id')
      },
      select: ['title', 'content', 'expiry', 'shortlink', 'mode']
    });

    if (!post){
      throw 'notFound';
    }
    //  else{
    //   var date = new Date;
    //   var expiry = post.createdAt + post.expiry * 1000;
    //   if (expiry < date.getTime()){
    //     await sails.helper.removePost(post.id);
    //     throw 'notFound';
    //   }
    // }

    // Respond with view.
    return {
      title: post.title,
      post: post.content
    };

  }


};
