module.exports = {


  friendlyName: 'Get post by shortlink',


  description: '',


  inputs: {

    shotlink: {
      type: 'string',
      required: true,
      isNotEmptyString: true
    }

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'Post not found.'
    }
  },


  fn: async function (inputs) {

    // eslint-disable-next-line no-undef
    var post = Post.findOne({
      shortlink: inputs.shortlink
    });

    if (!post){ throw 'notFound'; }

    // All done.
    return post;

  }


};
