module.exports = {


  friendlyName: 'Create post',


  description: '',


  inputs: {

    title: {
      type: 'string',
      requried: true,
      maxLength: 300,
      isNotEmptyString: true,
    },

    content: {
      type: 'string',
      requried: true,
      isNotEmptyString: true,
    },

    expiry: {
      type: 'number',
      description: 'Expiry time in seconds',
      required: true,
      isInteger: true,
      max: 2592000,
      min: 3600,
    },

    mode: {
      type: 'string',
      defaultsTo: 'txt',
      maxLength: 20
    },

  },


  exits: {

  },


  fn: async function (inputs) {
    var postData = inputs;

    var expireAt = _.now() + postData.expiry*1000;
    var ip = this.req.ip;

    let shortlink = await sails.helpers.generateShortlink();

    // eslint-disable-next-line no-undef
    let post = await Post.create({
      title: postData.title,
      content: postData.content,
      expiry: postData.expiry,
      shortlink: shortlink,
      expireAt: expireAt,
      mode: postData.mode,
      ip: ip
    })
    .intercept( (err) => {
      console.log(err);
      return err.message;
    }).fetch();

    // All done.
    return {
      shortlink: shortlink,
      expireAt: post.expireAt,
    };

  }


};
