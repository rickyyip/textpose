module.exports = {


  friendlyName: 'Get post list',


  description: '',


  inputs: {

    page: {
      type: 'number',
      defaultsTo: 1,
    },

    show: {
      type: 'number',
      defaultsTo: 50,
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    var page = inputs.page;
    var show = inputs.show;

    if (!page){
      page = 1;
    }

    if (!show){
      show = 1;
    }

    // eslint-disable-next-line no-undef
    var posts = await Post.find({
      limit: show,
      skip: (page-1)*show
    });

    // All done.
    return posts;

  }


};
