module.exports = {


  friendlyName: 'View read post',


  description: 'Display "Read post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/read-post'
    },

    notFound: {
      responseType: 'notFound',
    },

  },


  fn: async function () {
    // eslint-disable-next-line no-undef
    var post = await Post.findOne({
      where: {
        shortlink: this.req.param('id')
      },
      select: ['title', 'content', 'expiry', 'expireAt', 'shortlink', 'mode']
    });

    if (!post){
      throw 'notFound';
    }
    // else{
    //   var date = new Date;
    //   var expiry = post.createdAt + post.expiry * 1000;
    //   if (expiry < date.getTime()){
    //     await sails.helper.removePost(post.id);
    //     throw 'notFound';
    //   }
    // }

    var qrcode = await sails.helpers.genQr(sails.config.custom.baseUrl+'/p/'+post.shortlink);

    // Respond with view.
    return {
      formData: post,
      baseUrl: sails.config.custom.baseUrl,
      qrcode: qrcode
    };

  }


};
