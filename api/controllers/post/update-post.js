module.exports = {


  friendlyName: 'Update post',


  description: '',


  inputs: {

    shortlink: {
      type: 'string',
      requried: true
    },

    title: {
      type: 'string',
      requried: true,
      maxLength: 300,
      isNotEmptyString: true,
    },

    content: {
      type: 'string',
      requried: true,
      isNotEmptyString: true,
    },

    expiry: {
      type: 'number',
      description: 'Expiry time in seconds',
      required: true,
      isInteger: true,
      max: 2592000,
      min: 3600,
    },

    mode: {
      type: 'string',
      defaultsTo: 'text',
      maxLength: 20
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound'
    },

    usageUrror: {
      responseType: 'usageError'
    }

  },


  fn: async function (inputs) {

    // eslint-disable-next-line no-undef
    var post = await Post.findOne({shortlink: inputs.shortlink});

    if (!post){ throw 'notFound'; }
    // if (post.shortlink !== inputs.shortlink){ throw 'usageUrror'; }

    var timenow = new Date().getTime();

    var params = inputs;
    params.expireAt = timenow + params.expiry * 1000;

    // eslint-disable-next-line no-undef
    await Post.updateOne({id: post.id})
    .set(params);

    // All done.
    return {
      shortlink: post.shortlink,
    };

  }


};
