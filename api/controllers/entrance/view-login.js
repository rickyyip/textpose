module.exports = {


  friendlyName: 'View login',


  description: 'Display "Login" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/entrance/login',
    },

    redirect: {
      description: 'The requesting user is already logged in.',
      responseType: 'redirect'
    }

  },


  fn: async function () {

    if (this.req.me) {
      throw {redirect: '/dashboard'};
    }

    var dev = false;
    // if ( process.env.NODE_ENV === 'development'){
    //   dev = true;
    // }


    return {
      dev: dev,
    };

  }


};
