module.exports = {


  friendlyName: 'View roadmap',


  description: 'Display "Roadmap" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/roadmap'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
