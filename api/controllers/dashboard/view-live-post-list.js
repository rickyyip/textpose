module.exports = {


  friendlyName: 'View live post list',


  description: 'Display "Live post list" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/live-post-list'
    }

  },


  fn: async function () {

    var page = this.req.param('page');
    var show = this.req.param('show');

    if (!page){ page = 1; }
    if (!show){ show = 50; }

    // eslint-disable-next-line no-undef
    var posts = await LivePost.find()
    .sort([
      { createdAt: 'DESC' }
    ])
    .skip( (page-1)*show )
    .limit( show );

    // eslint-disable-next-line no-undef
    var totalPostCount = await LivePost.count();

    // Respond with view.
    return {
      page: page,
      show: show,
      posts: posts,
      totalPostCount: totalPostCount
    };

  }


};
