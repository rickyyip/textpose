module.exports = {


  friendlyName: 'View edit post',


  description: 'Display "Edit post" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/edit-post'
    },

    notFound: {
      responseType: 'notFound'
    }

  },


  fn: async function () {

    // eslint-disable-next-line no-undef
    var post = await Post.findOne({shortlink: this.req.param('id')});

    if (!post){ throw 'notFound'; }

    // Respond with view.
    return {
      baseUrl: sails.config.custom.baseUrl,
      post: post
    };

  }


};
