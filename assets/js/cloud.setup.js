/**
 * cloud.setup.js
 *
 * Configuration for this Sails app's generated browser SDK ("Cloud").
 *
 * Above all, the purpose of this file is to provide endpoint definitions,
 * each of which corresponds with one particular route+action on the server.
 *
 * > This file was automatically generated.
 * > (To regenerate, run `sails run rebuild-cloud-sdk`)
 */

Cloud.setup({

  /* eslint-disable */
  methods: {"confirmEmail":{"verb":"GET","url":"/email/confirm","args":["token"]},"logout":{"verb":"GET","url":"/api/v1/account/logout","args":[]},"updatePassword":{"verb":"PUT","url":"/api/v1/account/update-password","args":["password"]},"updateProfile":{"verb":"PUT","url":"/api/v1/account/update-profile","args":["fullName","emailAddress"]},"updateBillingCard":{"verb":"PUT","url":"/api/v1/account/update-billing-card","args":["stripeToken","billingCardLast4","billingCardBrand","billingCardExpMonth","billingCardExpYear"]},"login":{"verb":"PUT","url":"/api/v1/entrance/login","args":["emailAddress","password","rememberMe","captcha"]},"signup":{"verb":"POST","url":"/api/v1/entrance/signup","args":["emailAddress","password","fullName"]},"sendPasswordRecoveryEmail":{"verb":"POST","url":"/api/v1/entrance/send-password-recovery-email","args":["emailAddress"]},"updatePasswordAndLogin":{"verb":"POST","url":"/api/v1/entrance/update-password-and-login","args":["password","token"]},"deliverContactFormMessage":{"verb":"POST","url":"/api/v1/deliver-contact-form-message","args":["emailAddress","topic","fullName","message"]},"getCaptcha":{"verb":"GET","url":"/api/v1/get-captcha","args":[]},"createPost":{"verb":"POST","url":"/api/v1/post/create-post","args":["title","content","expiry","mode"]},"updatePost":{"verb":"PATCH","url":"/api/v1/post/update-post","args":["shortlink","title","content","expiry","mode"]},"deletePost":{"verb":"DELETE","url":"/api/v1/post/delete-post","args":[]},"getRoomEditorCount":{"verb":"GET","url":"/api/v1/live-post/get-room-editor-count/:id?","args":[]},"getChanges":{"verb":"GET","url":"/api/v1/live-post/get-changes/:id?","args":[]},"createLivePost":{"verb":"POST","url":"/api/v1/live-post/create-live-post","args":["title","expiryLive","expirySave","mode","maxEditor","maxGuest"]},"updateLivePostContent":{"verb":"POST","url":"/api/v1/live-post/update-live-post-content/:id?","args":["content","step","editorlink"]},"disconnectGuest":{"verb":"POST","url":"/api/v1/live-post/disconnect-guest/:id?","args":["guestlink"]},"helloLivePost":{"verb":"POST","url":"/api/v1/live-post/hello-live-post/:id?","args":["editorlink","guestlink"]},"disconnectEditor":{"verb":"POST","url":"/api/v1/live-post/disconnect-editor/:id?","args":["editorlink","name"]},"saveLivePost":{"verb":"POST","url":"/api/v1/live-post/save-live-post/:id?","args":["editorlink"]},"editLivePost":{"verb":"PUT","url":"/api/v1/live-post/edit-live-post/:id?","args":["change","editorlink"]},"editLivePostTitle":{"verb":"PUT","url":"/api/v1/live-post/edit-live-post-title/:id?","args":["title","editorlink"]},"editLivePostMode":{"verb":"PUT","url":"/api/v1/live-post/edit-live-post-mode/:id?","args":["mode","editorlink"]}}
  /* eslint-enable */

});
