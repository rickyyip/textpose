parasails.registerPage('edit-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    syncing: false,
    formData: {},
    cloudError: '',
    cloudSuccess: false,
    formErrors: {},
    confirmingSubmit: false,

    editor: undefined,

    _csrf: '',

    noticeTitle: '',
    noticeMessage: '',
    noticeShown: false,
    copiedResponse: '',

    baseUrl: '',
    setMode: '',
  },

  watch: {
    setMode: function(){
      if (this.setMode !== ''){
        // eslint-disable-next-line no-undef
        let modelist = ace.require('ace/ext/modelist');
        let filePath = 'post.'+this.setMode;
        let mode = '';
        try{
          mode = modelist.getModeForPath(filePath).mode;
        }catch(err){
          console.log(err);
        }

        this.editor.session.setMode(mode);
        this.formData.mode = this.setMode;
      }
    },

    copiedResponse: async function(){
      if (this.copiedResponse !== ''){
        let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
        await sleep(1000);
        this.copiedResponse = '';
      }
    },

    cloudError: function(){
      if (this.cloudError !== ''){
        this.notify('Oops...', 'We could not process your request. Please try again.');
      }
    }
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.formData = this.post;
  },
  mounted: async function() {
    this.loadEditor();
    $('#notice').toast('dispose');
    this.setMode = this.formData.mode;
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    loadEditor: function(){
      // eslint-disable-next-line no-undef
      this.editor = ace.edit('editor');
      this.editor.setTheme('ace/theme/monokai');
      // this.editor.session.setMode('ace/mode/text');

      this.editor.setFontSize(14);
      this.editor.getSession().setNewLineMode('unix');
      this.editor.getSession().setUseWrapMode(true);
      // this.editor.setReadOnly(true);
      this.editor.setValue(this.formData.content, -1);
      this.editor.resize(true);

      this.editor.on('change', (e) => {
        this.confirmingSubmit = false;
      });
    },

    notify: function(title, message){
      this.noticeTitle = title;
      this.noticeMessage = message;
      this.noticeShown = true;

      $('#notice').toast('show');
    },

    submittedForm: async function(res) {
      this.cloudSuccess = true;

      let link = this.baseUrl+'/p/'+res.shortlink;
      this.notify('Posted!', '<a href="'+link+'">'+link+'</a>');

    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};

      let docLines = this.editor.getSession().getLength();

      if (docLines > 1000){
        this.formErrors.maxLines = true;
        console.log('Exceeded line limit. Current:', docLines);
        this.notify('Humm...', 'You have exceeded the line limit. Max: 1000 lines.');
      }

      this.formData.content = this.editor.getValue();

      const byteSize = str => new Blob([str]).size;
      let docSize = byteSize(this.formData.content);

      if ( docSize > 512000){
        this.formErrors.contentSize = true;
        console.log('Size exceeded limit. Current:', docSize);
        this.notify('Humm...', 'You have exceeded the size limit. Max: 512KB.');
      }

      var argins = this.formData;

      // Validate title:
      if(!argins.title) {
        this.formErrors.title = true;
        console.log('Missing post title!')
        this.notify('Hol\' up a minute...', 'The title is missing.');
      } else if (argins.title.length > 300){
        this.formErrors.title = true;
        console.log('Post title is too long!');
        this.notify('Humm...', 'The post title is too long!');
      }

      // Validate content:
      if(!argins.content) {
        this.formErrors.content = true;
        console.log('Cannot post a blank document!');
        this.notify('Hol\' up a minute... ', 'Cannot post a blank document!');
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      return argins;
    },
  }
});
