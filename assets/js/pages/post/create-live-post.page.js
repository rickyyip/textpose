parasails.registerPage('create-live-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    _csrf: '',

    syncing: false,
    formData: {
      expiryLive: 10800,
      expirySave: 604800,
      maxEditor: 10,
      maxGuest: 30,
    },
    cloudError: '',
    cloudSuccess: false,
    formErrors: {},

    modal: '',

    editLink: '',
    guestLink: ''
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    submittedForm: async function(res) {

      this.formData.editorlink = res.editorlink;
      this.formData.guestlink = res.guestlink;
      // console.log(this.formData);
      this.cloudSuccess = true;
      // let link = this.baseUrl+'/p/'+this.formData.shortlink;

      // this.notify('Posted!', '<a href="'+link+'">'+link+'</a>');
      this.goto(this.baseUrl+'/le/'+res.editorlink);

    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};

      // Copy from editor to formData
      // this.formData.content = this.editor.getValue();

      // Set argins for submission
      var argins = this.formData;

      // Validate title:
      if(!argins.title) {
        this.formErrors.title = true;
        console.log('Missing post title!');
        this.notify('Hol\' up a minute...', 'The title is missing.');
      } else if (argins.title.length > 300){
        this.formErrors.title = true;
        console.log('Post title is too long!');
        this.notify('Humm...', 'The post title is too long!');
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      // Return argins for submission
      return argins;
    },
  }
});
