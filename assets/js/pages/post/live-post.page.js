parasails.registerPage('live-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    _csrf: '',

    postID: '',
    guestlink: '',
    readonly: false,

    modal: '',
    guestQrcode: '',
    qrCodeToShow: '',
    qrCodeUrl: '',

    // Default settings
    defaultTheme: 'monokai',

    // Settings
    editorTheme: '',

    // Theme list
    themelist: [],

    // Editor
    editor: undefined,
    mobileMode: false,

    formData: {},
    cloudError: '',

    editorTitle: '',
    editorBody: '',
    editorMode: '',

    updateQueue: [],
    recentUpdates: [],

    globalStepUpto: 0,  // The step that the document is in sync with
    globalStepCounter: 0,   // The global step counter. If globalStepUpto is smaller than this, it means the document is not in sync with the latest state.

    syncingPost: true,
    socketConnected: false,
    guestCount: 0,
    lastChangeAt: 0,

    triggerLocalUpdate: 0,

    // Notify
    noticeTitle: '',
    noticeMessage: '',
    noticeShown: false,

    wsActivity: false,

    // Chat data and controls
    chatname: 'Guest',
    chatOpen: false,
    chatLoaded: false,
    newMessageCount: 0,
    messageHistory: [],
    messageTotal: 0,
    messageFrom: undefined,
    messageTo: undefined,

    // messageTextarea: '',
    // postingMessage: false,
  },

  watch: {

    editorTheme: async function(){
      await this.setEditorTheme(this.editorTheme);
    },

    triggerLocalUpdate: async function(){

      if (this.updateQueue.length > 0){
        console.log('Triggered local update. Queue:', this.updateQueue.length);

        let change = this.updateQueue.shift();

        if (change.step === this.globalStepUpto + 1){
          this.globalStepUpto += 1;
          await this.processUpdateBody(change);
          // this.triggerLocalUpdate += 1;
        } else if (change.step > this.globalStepUpto + 1){
          let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

          this.updateQueue.push(change);
          console.log('Got a change that is not the expected step. Putting change back in queue.');
          console.log(this.updateQueue);

          await sleep(100);
          this.triggerLocalUpdate += 1;
        }

      } else {
        console.log('Nothing left in queue.');
      }

    },

    localStepCounter: async function(){
      if (this.localStepCounter >= 50){
        await this.updateRemotePostContent();
        this.localStepCounter = 0;
      }
    }

  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.readonly = this.formData.readonly;
    this.editorTitle = this.formData.title;
    this.editorMode = this.formData.mode;
    this.editorBody = this.formData.content;
    this.postID = this.formData.id;
  },

  mounted: async function() {
    await this.windowResizer();
    // Hide toast
    $('#notice').toast('dispose');

    // Add window resize event listener
    await this.windowResizer();

    await this.loadEditor();

    await this.applySavedSettings();

    // Monitor socket connection
    io.socket.on('connect', async () => {
      this.socketConnected = true;
      await this.loadChanges();
      if (!this.readonly){
        await this.forceDocumentResync();
      }
      await this.updatedPostMode();
    });

    io.socket.on('disconnect', async () => {
      this.socketConnected = false;
    });

    window.addEventListener('beforeunload', (event) => {
      // Cancel the event as stated by the standard.
      // event.preventDefault();

      this.disconnect();
      // Chrome requires returnValue to be set.
      event.returnValue = '';

    });


    if (!this.readonly){
      this.setSocketEvents();
      await this.joinLivePostSocket();
    }

    this.getMessages();

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    windowResizer: async function(){
      // Initial resizing
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);

      // Then listen to event
      window.addEventListener('resize', () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
      });
    },

    setSocketEvents: async function(){
      io.socket.on('hello-guest', async (data) => {
        this.wsActive();
        console.log('Someone joined the party!');
        this.guestCount += 1;
      });

      io.socket.on('updateBody', async (data) => {
        // Data is a json object. No parsing needed.
        this.wsActive();
        // console.log('Got update body event.');

        this.recentUpdates.push(data);
        if (data.step > this.globalStepCounter){
          this.globalStepCounter = data.step;
        }

        this.recentUpdates.push(data);
        this.updateQueue.push(data);

        this.triggerLocalUpdate += 1;

      });

      io.socket.on('updateTitle', async (data) => {
        this.wsActive();
        this.editorTitle = data.title;
        document.title = data.title;
      });

      io.socket.on('updateMode', async (data) => {
        this.wsActive();
        this.editorMode = data.mode;
        this.changeEditorMode(data.mode);
      });

      io.socket.on('guestLeft', async (data) => {
        this.wsActive();
        this.guestCount -= 1;
        console.log('Someone has left.');
      });

      io.socket.on('newMessage', async (data) => {
        this.wsActive();
        await this.pushMessageToHistory({
          type: data.type,
          time: data.time,
          from: data.from,
          body: data.body,
        });
        if (!this.chatOpen){
          this.newMessageCount += 1;
        }
      });

      console.log('Socket events ready.');
    },

    changeEditorMode: function(setMode){
      // eslint-disable-next-line no-undef
      let modelist = ace.require('ace/ext/modelist');
      let filePath = 'post.'+setMode;
      let mode = '';
      try{
        mode = modelist.getModeForPath(filePath).mode;
      }catch(err){
        console.log(err);
      }

      this.editor.session.setMode(mode);
    },

    updatedPostMode: function(){
      this.changeEditorMode(this.editorMode);
    },


    loadEditor: function(){
      // eslint-disable-next-line no-undef
      this.editor = ace.edit('editor');
      this.editor.setTheme('ace/theme/monokai');
      this.editor.session.setMode('ace/mode/text');
      this.editor.setFontSize(14);

      this.editor.getSession().setNewLineMode('unix');
      this.editor.getSession().setUseWrapMode(true);
      this.editor.resize(true);
      this.editor.setReadOnly(true);
    },

    loadChanges: async function(){

      this.socketConnected = true;

      // Clear document
      this.editorBody = '';

      console.log('Fetching changes');
      await io.socket.get('/api/v1/live-post/get-changes/'+this.postID, async (data, jwRes) => {
        this.wsActive();
        if (jwRes.statusCode < 400){

          let changes = data.changes;
          let globalStep = data.globalStep;
          let content = data.content;
          let contentStep = data.contentStep;

          // Update title, mode, editor count
          this.editorTitle = data.title;
          this.editorMode = data.mode;
          this.submittedBody = data.content;

          // Update counters
          this.globalStepCounter = globalStep;
          this.globalStepUpto = contentStep;

          // If there is no global step or cache content step, nothing is typed yet.
          if (!globalStep && !contentStep){
            return;
          }

          console.log('Total steps:', globalStep);
          console.log('Cache content step:', contentStep);

          // Apply cache contents
          this.editor.setValue(content, -1);

          // Apply changes if there is any
          if (changes){
            console.log('Received changes after cache:', changes.length);
            console.log('Expected changes after cache:', globalStep - contentStep);
            for (let i=0; i < changes.length; i++){
              // await this.processUpdateBody(JSON.parse(changes[i]));
              let change = JSON.parse(changes[i]);
              if (change.step > this.globalStepUpto){
                // this.recentUpdates.push(JSON.parse(changes[i]));
                this.updateQueue.push(JSON.parse(changes[i]));
              }
            }
          }

          this.triggerLocalUpdate += 1;

          return;

        } else {
          console.log('Error fetching changes:',jwRes.statusCode);
        }
      });

    },

    forceDocumentResync: async function(){
      if (this.syncingPost || this.readonly){ return; }
      console.log('Force resync document:', this.syncingPost);
      $('#notice').toast('dispose');

      try{
        io.socket.reconnect();
      }catch(err){
        console.log('Socket is already connected.');
      }

      // Force a resync
      this.syncingPost = true;
      await this.loadChanges();
      this.updatedPostMode();
    },

    joinLivePostSocket: async function(){
      let params = {
        _csrf: this._csrf,
        guestlink: this.guestlink,
      };

      io.socket.post('/api/v1/live-post/hello-live-post/'+this.postID, params, (data, jwRes) => {
        if (jwRes.statusCode < 400){
          console.log('Joined room!');
        } else {
          console.log('Error saying hello:',jwRes.statusCode);
        }
      });

    },

    processUpdateBody: async function(entry){
      if (!entry){ return; }
      console.log('Processing step:', entry.step);
      // this.globalStepUpto += 1;

      // Check step
      // If entry step is not the step after the globalStepUpto, check updateQueue for the step
      if (entry.step !== this.globalStepUpto){
        console.log('WARNING! Processing step is not the next step expected.');
        console.log('Processing step:', entry.step,'. Expected:', this.globalStepUpto);
        this.notify('Warning', 'Some contents may be out of sync. Please try the resync button.');
      }

      // Apply change and update step count
      // this.globalStepUpto = entry.step;
      let e = entry.change;

      if (e.action === 'insert'){

        let lines = '';
        for (let i=0; i < e.lines.length; i++){
          lines += e.lines[i];
          if (i !== e.lines.length - 1){
            lines += '\n';
          }
        }

        this.editor.session.insert(e.start, lines);

      } else if (e.action === 'remove'){
        let range = {
          start: e.start,
          end: e.end,
        };

        this.editor.session.remove(range);
      }

      // Trigger local update after body is updated.
      this.triggerLocalUpdate += 1;
      return;

    },

    // Settings
    applySavedSettings: async function(){
      //eslint-disable-next-line no-undef
      let themelist = await ace.require('ace/ext/themelist');
      // let themes = await themelist.themesByName;
      this.themelist = themelist.themesByName;
      console.log(this.themelist);

      let theme = await this.getCookie('editor_theme');
      console.log('Got theme:', theme);
      if (theme){
        this.editorTheme = theme;
      } else {
        this.editorTheme = this.defaultTheme;
      }

      // await this.setEditorTheme(this.editorTheme);

    },

    setEditorTheme: async function(theme){
      if (!theme){ return; }
      console.log('Setting theme:', theme);

      this.setCookie('editor_theme', theme);
      this.editor.setTheme('ace/theme/'+theme);
    },

    setCookie: async function(name, data){
      document.cookie = `${name}=${data}; max-age=315360000; path=/; SameSite=Lax`;
    },

    getCookie: async function(name){
      const cookies = document.cookie.split('; ');

      console.log(cookies);
      
      let index = _.findIndex(cookies, (item) => {
        return item.startsWith(`${name}=`);
      });

      if (index >= 0){
        return cookies[index].split('=')[1];
      } else {
        return '';
      }
    },

    notify: function(title, message){
      this.noticeTitle = title;
      this.noticeMessage = message;
      this.noticeShown = true;

      $('#notice').toast('show');
    },

    disconnect: async function(){
      if (this.readonly){ return; }

      let params = {
        guestlink: this.guestlink,
        _csrf: this._csrf
      };

      await io.socket.post('/api/v1/live-post/disconnect-guest/'+this.postID, params, (data, jwRes) => {
        if (jwRes.statusCode < 400 ){
          console.log('Disconnected');
          return;
        } else {
          console.log('Error disconnecting:', jwRes.statusCode);
        }
      });

      return;
    },

    copyLinkToClipboard: function(link){
      const el = document.createElement('textarea');
      el.value = this.baseUrl + link;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      // this.copiedResponse = 'Copied!';
    },

    openQrCode: function(type){
      if (type ==='guest'){
        this.qrCodeToShow = this.guestQrcode;
        this.qrCodeUrl = this.baseUrl+'/l/'+this.guestlink;
        this.modal = 'qrcode';
      }
    },

    closeQrCode: function(){
      this.modal = '';
    },

    openSettingsModal: function(){
      this.modal = 'settings';
    },

    closeSettingsModal: function(){
      this.modal = '';
    },

    wsActive: async function(){
      this.wsActivity = true;
      await this.sleep(50);
      this.wsActivity = false;
    },

    sleep: function(ms){
      return new Promise(resolve => setTimeout(resolve, ms));
    },

    // Chat functions
    toggleChat: async function(){
      this.chatOpen = !this.chatOpen;
      await this.sleep(100);
      this.editor.resize();
    },

    pushMessageToHistory: async function(message){
      this.messageHistory.push(message);
    },

    prependMessageToHistory: async function(message){
      this.messageHistory.unshift(message);
    },

    getMessages: async function(append = true, from, to){
      if (from > to){ 
        console.log('Invalid arguments.');
        return; }

      let params = {
        from: from,
        to: to,
      };

      if (append && from && to){
        if (params.from <= this.messageTo && this.messageTo < this.messageTotal - 1){
          params.from = this.messageTo + 1;
        }
        if (this.messageTo < this.messageTotal - 1){
          params.to = this.messageTo + 1;
        }
      }

      // if (params.from >= this.messageFrom && this.messageFrom > 0){ params.from = this.messageFrom - 1; }
      // if (params.to <= this.messageTo && this.messageTo < this.messageTotal - 1){ params.to = this.messageTo + 1; }

      console.log('Fetching messages from: ' + from + ' to: ', to);

      let url = '/api/v1/chat/get-messages/'+ this.postID;

      await io.socket.get(url, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400){
          this.messageTotal = data.total;

          if (this.messageFrom > data.from || this.messageFrom === undefined){
            this.messageFrom = data.from;
          }

          if (this.messageTo < data.to || this.messageTo === undefined){
            this.messageTo = data.to;
          }

          console.log('Total messages:', this.messageTotal);
          console.log('Loaded messages from ID:', this.messageFrom,'to',this.messageTo);

          let messages = data.messages;

          if (!messages.length){
            messages = [messages];
          }

          this.messageTextarea = '';
          if (append){
            for (let i=0; i < messages.length; i++){
              let message = JSON.parse(messages[i]);
              await this.pushMessageToHistory({
                type: message.type,
                from: message.from,
                time: message.time,
                body: message.body,
              });
            }
            this.scrollChatToBottom();
          } else {
            for (let i=messages.length-1; i>=0; i--){
              let message = JSON.parse(messages[i]);
              await this.prependMessageToHistory({
                type: message.type,
                from: message.from,
                time: message.time,
                body: message.body,
              });
            }
          }

          this.chatLoaded = true;

        } else {
          console.log('Error fetching messages:', jwRes.body);
        }
      });
    },

    scrollChatToBottom: function(){
      let div = document.getElementById('chat-history');
      // div.scrollTop = div.scrollHeight - div.clientHeight;
      $('#chat-history').animate({
        scrollTop: div.scrollHeight - div.clientHeight
      }, 200);
    },

    loadEarlierMessages: async function() {
      if (this.messageFrom <= 0){
        return;
      }

      let to = this.messageFrom - 1;
      let from = this.messageFrom - 25;

      if (from < 0){
        from = 0;
      }

      console.log('Pressed button to load: '+from+' to '+to);

      await this.getMessages(false, from, to);

    }

  }
});
