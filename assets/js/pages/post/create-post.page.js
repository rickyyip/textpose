parasails.registerPage('create-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    modal: '',
    _csrf: '',

    syncing: false,
    formData: {
      expiry: 604800, // 7 days
      mode: 'txt'
    },
    setMode: 'txt',
    // setExpiry: 0,

    posted: false,
    expired: false,

    cloudError: '',
    cloudSuccess: false,
    formErrors: {},
    confirmingSubmit: false,

    shortlink: '',
    editor: undefined,

    noticeTitle: '',
    noticeMessage: '',
    noticeShown: false,
    copiedResponse: '',

    datetime: undefined,
    baseUrl: '',

    toggleHiddenTextarea: false,

    // Local DB params
    supportIndexedDB: undefined,
    db: undefined,
    localPosts: [],
    currentPostUUID: '',

    // Local DB states
    localPostSaved: true,
    dbReady: false,
    forceNewPost: false,

    editorReady: false,
    textareaContent: '',
    mobileMode: false,

    // Settings
    appliedSettings: false,
    editorTheme: 'monokai',
    editorTabSize: 4,
    editorSoftTabs: true,

    // Theme list
    themelist: [],
  },

  watch: {

    editorTheme: async function(){
      if (!this.appliedSettings){ return; }
      await this.setEditorTheme(this.editorTheme);
    },

    editorTabSize: async function(){
      if (!this.appliedSettings){ return; }
      await this.setEditorTabSize(this.editorTabSize);
    },

    editorSoftTabs: async function(){
      // console.log('Editor soft tabs is now =', this.editorSoftTabs);
      if (!this.appliedSettings){ return; }
      await this.setEditorSoftTabs(this.editorSoftTabs);
    },

    setMode: function(){
      if (this.setMode !== ''){
        // eslint-disable-next-line no-undef
        let modelist = ace.require('ace/ext/modelist');
        let filePath = 'post.'+this.setMode;
        let mode = '';
        try{
          mode = modelist.getModeForPath(filePath).mode;
        }catch(err){
          console.log(err);
        }

        this.editor.session.setMode(mode);
        this.formData.mode = this.setMode;
      }
    },

    // setExpiry: function(){
    //   if (this.editorReady && this.setExpiry > 0 && this.setExpiry !== this.formData.expiry){
    //     // console.log('Set expiry changed:', this.setExpiry);
    //     this.formData.expiry = this.setExpiry;
    //     // Auto-save
    //     this.updateLocalPost();
    //   }
    // },

    copiedResponse: async function(){
      if (this.copiedResponse !== ''){
        let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
        await sleep(1000);
        this.copiedResponse = '';
      }
    },

    cloudError: function(){
      if (this.cloudError !== ''){
        this.notify('Oops...', 'We could not process your request. Please try again.');
      }
    },

    editorReady: function(){
      if (this.editorReady){
        this.monitorEditor();
      }
    },

    textareaContent: function(){
      this.formData.content = this.textareaContent;
      if (this.mobileMode){
        this.updateLocalPost();
      }
    },

    mobileMode: function(){
      // Make sure textarea and editor contents are in sync
      if (this.mobileMode){
        this.textareaContent = this.editor.getValue();
        // this.editor.destroy();
      } else {
        // this.loadEditor();
        // this.formData.content = this.textareaContent;
        this.editor.setValue(this.formData.content, -1);
      }
    }
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.isForceNewPost();
    // this.formData.title = 'New Post at '+new Date().toISOString();
    if (!this.formData.title){
      this.formData.title = 'Untitled';
    }

    // Detect indexedDB support
    this.supportIndexedDB = true;
    let test = window.indexedDB.open('test');
    test.onerror = () => {
      console.log('This browser does not support IndexedDB');
      this.supportIndexedDB = false;
    };

    if (this.supportIndexedDB){
      this.forceNewPost? null: this.getPostUUID();
      this.startLocalDB();
    }
  },
  mounted: async function() {
    await this.loadEditor();

    await this.applySavedSettings();

    $('#notice').toast('dispose');
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    startLocalDB: function(){
      if (!this.supportIndexedDB){ return; }

      // Local DB operations
      let operation = window.indexedDB.open('textpose_local', 2);

      operation.onsuccess = () => {
        console.log('Open local DB successful!');
        this.db = operation.result;
        // If db is opened, start loading data.
        this.loadLocalData();
        this.dbReady = true;
      };

      operation.onupgradeneeded = (e) => {
        console.log('Upgrading local DB...');
        this.db = e.target.result;


        if (e.oldVersion < 1){
          let store = this.db.createObjectStore('posts', {keyPath: 'uuid'});
          store.createIndex('uuid', 'uuid', {unique: true});
          store.createIndex('title', 'title', {unique: false});
          store.createIndex('content', 'content', {unique: false});
          store.createIndex('mode', 'mode', {unique: false});
          store.createIndex('expiry', 'expiry', {unique: false});
          store.createIndex('shortlink', 'shortlink', {unique: false});
          store.createIndex('posted', 'posted', {unique: false});
          store.createIndex('updatedAt', 'updatedAt', {unique: false});
        } else if (e.oldVersion < 2){
          let store = operation.transaction.objectStore('posts');
          store.createIndex('expireAt', 'expireAt', {unique: false});
        }

        console.log('DB upgrade complete.');
      };

    },

    loadLocalData: function(){
      let store = this.db.transaction('posts').objectStore('posts');
      let postCount = 0;
      let lastUpdated = 0;
      let loadPostIndex = 0;
      let found = false;

      // Clear local posts history
      this.localPosts = [];

      console.log('Loading local data...');
      store.index('updatedAt').openCursor().onsuccess = (e) => {
        let cursor = e.target.result;

        if (cursor){
          postCount += 1;
          // console.log(postCount+':',cursor.value);
          this.localPosts.push(cursor.value);

          // Find an existing local post if a currentPostUUID is set via query
          if (this.currentPostUUID && this.currentPostUUID === cursor.value.uuid){
            console.log('Target local post is found!');
            loadPostIndex = postCount - 1;
            found = true;
            // If no currentPostUUID is set, find the last updated post
          } else if (!this.forceNewPost && !this.currentPostUUID){
            if (cursor.value.updatedAt > lastUpdated){
              lastUpdated = cursor.value.updatedAt;
              loadPostIndex = postCount - 1;
              found = true;
            }
          }

          // Continue fetching local posts
          cursor.continue();
        } else {
          // All posts are loaded at this point
          console.log('All posts loaded.');
          if (this.forceNewPost || !postCount){
            // If force creating a new post or there is no existing post, create a new empty post
            console.log('No post found. Creating a new empty post and reload...');
            this.createNewPostEntry();
            this.editorReady = true;
          } else {
            console.log('Found',postCount,'posts.');
            // console.log('localPosts:',this.localPosts);

            if (found){
              // If a target local post is found, load it to formData, textareaContent and editor
              this.formData = this.localPosts[loadPostIndex];
              this.textareaContent = this.formData.content;
              this.setMode = this.formData.mode;
              // this.setExpiry = this.formData.expiry;
              this.editor.setValue(this.formData.content, -1);
              if (this.formData.posted){
                this.editor.setReadOnly(true);
                this.posted = true;
              }

              // Update page title
              document.title = this.formData.title;

              // Everything is loaded and ready for editing.
              console.log('Loaded post:',this.formData.uuid);
              window.history.replaceState('', 'this.formData.title' , '/?local='+this.formData.uuid);
              // window.history.pushState('', this.formData.title , '/?local='+this.formData.uuid);
              this.editorReady = true;

            } else {
              console.log('Post not found for UUID:', this.currentPostUUID);
              this.createNewPostEntry();
              this.editorReady = true;
            }
          }
        }

      };
    },


    updateLocalPost: async function(){
      // Do nothing if IndexedDB is not supported
      if (!this.supportIndexedDB){ return; }

      // console.log('Updating post:',this.formData);
      let action = await this.db.transaction(['posts'], 'readwrite');
      let store = await action.objectStore('posts');


      let value = this.formData;
      value.updatedAt = new Date().getTime();

      // Update local post
      let req = await store.put(value);

      req.onsuccess = async () => {
        // console.log('PUT post request successful.');
      };

      action.oncomplete = async () => {
        // console.log('Post updated:', this.formData.uuid);
        this.localPostSaved = true;
      };
    },

    removeLocalPost: async function(uuid){
      console.log('Removing post:',uuid);

      let action = await this.db.transaction(['posts'], 'readwrite');
      let store = await action.objectStore('posts');
      await store.delete(uuid);

      action.oncomplete = async () => {
        console.log('Post:',uuid,'is deleted.');
        await this.loadLocalData();
      };
    },

    removeAllLocalPosts: async function(){
      let store = await this.db.transaction('posts').objectStore('posts');
      let postCount = 0;

      console.log('Loading local data...');
      store.index('updatedAt').openCursor().onsuccess = async (e) => {
        let cursor = await e.target.result;

        if (cursor){
          postCount += 1;

          await this.removeOneLocalPost(cursor.value.uuid);

          // Continue fetching local posts
          cursor.continue();
        } else {
          // All posts are removed at this point
          console.log('All posts removed.',postCount,'posts in total.');
          await this.loadLocalData();
        }

      };
    },

    removeAllSubmittedPosts: async function(){
      // Save any local chaneges
      await this.updateLocalPost();

      let store = await this.db.transaction('posts').objectStore('posts');
      let postCount = 0;

      console.log('Loading local data...');
      store.index('updatedAt').openCursor().onsuccess = async (e) => {
        let cursor = await e.target.result;

        if (cursor){

          if (cursor.value.posted){
            await this.removeOneLocalPost(cursor.value.uuid);
            postCount += 1;
          }

          // Continue fetching local posts
          cursor.continue();
        } else {
          // All posts are removed at this point
          console.log('All submitted posts removed.',postCount,'posts in total.');
          if (this.formData.posted){
            this.goto('/');
          } else {
            await this.loadLocalData();
          }
        }

      };
    },

    removeOneLocalPost: async function(uuid){
      let action = await this.db.transaction(['posts'], 'readwrite');
      let store = await action.objectStore('posts');
      await store.delete(uuid);
    },

    createNewPostEntry: async function(){
      let emptyPost = {
        // eslint-disable-next-line no-undef
        uuid: uuidv4(),
        title: 'Untitled',
        content: '',
        mode: '',
        expiry: this.formData.expiry,
        expireAt: undefined,
        shortlink: '',
        posted: false,
        updatedAt: new Date().getTime(),
      };

      let action = await this.db.transaction(['posts'], 'readwrite');
      let store = await action.objectStore('posts');

      let request = await store.add(emptyPost);
      request.onsuccess = () => {
        console.log('ADD post request successful.');
      };

      action.oncomplete = async () => {
        console.log('Post saved:', emptyPost.uuid);
        // Set formData
        this.formData = emptyPost;
        await this.editor.setValue('', -1);
        await this.localPosts.push(emptyPost);
        await window.history.pushState('', '', '/?local='+this.formData.uuid);
      };

      action.onerror = async (e) => {
        console.log('Error saving post:',e);
      };

    },

    getPostUUID: function(){
      const query = window.location.search;
      const params = new URLSearchParams(query);

      const uuid = params.get('local');
      if (uuid){
        this.currentPostUUID = uuid;
      }
    },

    isForceNewPost: function(){
      const query = window.location.search;
      const params = new URLSearchParams(query);

      const newPost = params.get('new');
      if (newPost){
        this.forceNewPost = true;
      }
    },

    loadEditor: function(){
      // eslint-disable-next-line no-undef
      this.editor = ace.edit('editor');
      // this.editor.setTheme('ace/theme/monokai');
      this.editor.session.setMode('ace/mode/text');
      this.editor.setFontSize(14);

      this.editor.session.setNewLineMode('unix');
      this.editor.session.setUseWrapMode(true);
      this.editor.resize(true);
    },

    monitorEditor: async function(){
      this.editor.on('change', async (e) => {
        // Update content status
        this.localPostSaved = false;
        this.confirmingSubmit = false;
        this.localSaved = false;
        // Copy from editor to formData
        this.formData.content = await this.editor.getValue();
        // Auto-save
        await this.updateLocalPost();
      });
    },

    // Settings
    applySavedSettings: async function(){
      //eslint-disable-next-line no-undef
      let themelist = await ace.require('ace/ext/themelist');
      // let themes = await themelist.themesByName;
      this.themelist = await themelist.themesByName;
      // console.log(this.themelist);

      let theme = await this.getCookie('editor_theme');
      console.log('Got theme:', theme);
      if (theme){
        this.editorTheme = theme;
      }
      await this.setEditorTheme(this.editorTheme);

      let tabSize = await this.getCookie('editor_tab_size');
      console.log('Got tabSize:', tabSize);
      if (tabSize){
        this.editorTabSize = tabSize;
      }
      await this.setEditorTabSize(this.editorTabSize);

      let softTabs = await this.getCookie('editor_soft_tabs');
      console.log('Got softTabs:', softTabs);
      if (softTabs !== undefined){
        this.editorSoftTabs = softTabs;
      }
      await this.setEditorSoftTabs(this.editorSoftTabs);

      //Done applying settings
      this.appliedSettings = true;
    },

    setEditorTheme: async function(theme){
      console.log('Setting theme:',theme);
      if (!theme){ return; }
      await this.editor.setTheme('ace/theme/'+theme);
      await this.setCookie('editor_theme', theme);
    },

    setEditorTabSize: async function(size){
      console.log('Setting tab size:',size);
      if (!size){ return; }
      await this.editor.session.setTabSize(size);
      await this.setCookie('editor_tab_size', size);
    },

    setEditorSoftTabs: async function(arg){
      console.log('Setting soft tabs:',arg);
      await this.editor.session.setUseSoftTabs(arg);
      await this.setCookie('editor_soft_tabs', arg);
    },

    // Misc
    setCookie: async function(name, data){
      document.cookie = `${name}=${data}; max-age=315360000; path=/; SameSite=Lax`;
      // console.log(name,':',data);
    },

    getCookie: async function(name){
      const cookies = document.cookie.split('; ');

      let value = undefined;
      for (let i=0; i < cookies.length; i++){
        if (cookies[i] === ''){ continue; }
        let item = cookies[i].split('=')[0];
        if (item === name){
          value = cookies[i].split('=')[1];
          break;
        }
      }

      return value;
    },

    openSettingsModal: async function(){
      if (!this.appliedSettings){ return; }
      this.modal = 'settings';
    },

    closeSettingsModal: async function(){
      this.modal = '';
    },

    notify: async function(title, message){
      this.noticeTitle = title;
      this.noticeMessage = message;
      this.noticeShown = true;

      $('#notice').toast('show');
    },

    updatePostTitle: async function(){
      document.title = this.formData.title;
      await this.updateLocalPost();
    },

    clearTitle: async function(){
      document.getElementById('createPostTitle').value = '';
    },

    openTextarea: async function(){
      this.modal = 'textarea';
      var textarea = document.getElementById('popup-textarea');
      if (textarea){
        textarea.focus();
      }
    },

    closeTextarea: async function(){
      this.modal = '';
    },

    // pasteFromTextarea: function(){
    //   var value = document.getElementById('popup-textarea').value;

    //   var cursorPosition = this.editor.getCursorPosition();
    //   this.editor.session.insert(cursorPosition, value);
    //   // this.editor.setValue(value, 1);

    //   this.modal = '';
    //   document.getElementById('popup-textarea').value = '';
    // },

    copyLinkToClipboard: async function(){
      const el = document.createElement('textarea');
      el.value = this.baseUrl + '/p/' + this.formData.shortlink;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      this.copiedResponse = 'Copied!';
    },

    submittedForm: async function(res) {
      this.editor.setReadOnly(true);
      this.formData.shortlink = res.shortlink;
      console.log(this.formData.shortlink);
      this.cloudSuccess = true;
      let link = this.baseUrl+'/p/'+this.formData.shortlink;

      this.formData.posted = true;
      this.formData.expireAt = _.now() + this.formData.expiry * 1000;
      await this.updateLocalPost();
      await this.notify('Posted!', '<a href="'+link+'">'+link+'</a>');
    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};

      // If editor is not in focus, or if editor is not in sync with textarea, copy content to editor
      if (!this.editor.isFocused() || this.formData.content !== this.editor.getValue()){
        this.editor.setValue(this.formData.content, 1);
      }
      // Check content length
      // let docLines = this.editor.getSession().getLength();

      // if (docLines > 1000){
      //   this.formErrors.maxLines = true;
      //   console.log('Exceeded line limit. Current:', docLines);
      //   this.notify('Humm...', 'You have exceeded the line limit. Max: 1000 lines.');
      // }

      // Check content size
      const byteSize = str => new Blob([str]).size;
      let docSize = byteSize(this.editor.getValue());

      if ( docSize > 512000){
        this.formErrors.contentSize = true;
        console.log('Size exceeded limit. Current:', docSize);
        this.notify('Humm...', 'You have exceeded the size limit. Max: 512KB.');
      }

      // Copy from editor to formData
      // this.formData.content = this.editor.getValue();

      if (this.formData.mode === ''){
        this.formData.mode = 'txt';
      }

      // Set argins for submission
      var argins = this.formData;

      // Validate title:
      if(!argins.title) {
        this.formErrors.title = true;
        console.log('Missing post title!');
        this.notify('Hol\' up a minute...', 'The title is missing.');
      } else if (argins.title.length > 300){
        this.formErrors.title = true;
        console.log('Post title is too long!');
        this.notify('Humm...', 'The post title is too long!');
      }

      // Validate content:
      if(!argins.content) {
        this.formErrors.content = true;
        console.log('Cannot post a blank document!');
        this.notify('Hol\' up a minute... ', 'Cannot post a blank document!');
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      // Return argins for submission
      return argins;
    },
  }
});
