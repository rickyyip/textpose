parasails.registerPage('edit-live-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    _csrf: '',

    // Page info
    guestlink: '',
    editorlink: '',
    postID: '',

    formData: {},
    // formErrors: {},
    cloudError: '',

    modal: '',
    guestQrcode: '',
    editorQrcode: '',
    qrCodeToShow: '',
    qrCodeUrl: '',

    // Editor objects and properties
    editor: undefined,
    textareaContent: '',
    mobileMode: false,

    // Editor form data
    editorTitle: '',
    editorBody: '',
    editorMode: '',

    // A copy of text with changes that are acknowledged by the server
    submittedBody: '',

    // Queues of changes
    updateQueue: [],
    updateQueueHold: [],
    recentUpdates: [],
    historyQueue: [],
    // updateRemoteQueue: [],

    // Triggers of queue processors
    triggerLocalUpdate: 0,
    triggerUpdateRemoteCache: false,
    // triggerRemoteUpdate: 0,

    // Status of queues
    processingRecentUpdate: false,
    processingUpdateQueue: false,
    // processingUpdateRemoteQueue: false,
    sleepDelay: 10,

    // Changes counter
    submitStepUpTo: 0,
    globalStepUpto: 0,  // The step that the document is in sync with
    globalStepCounter: 0,   // The global step counter. If globalStepUpto is smaller than this, it means the document is not in sync with the latest state.
    localStepCounter: 0,  // How many steps that are produced locally

    // Page and connection status
    syncingPost: true,
    cleaningUp: false,
    socketConnected: false,
    reconnecting: false,
    wsActivity: false,
    editorCount: 0,
    lastLocalActiveAt: 0,
    lastRemoteActiveAt: 0,
    unloaded: false,

    // Notify
    noticeTitle: '',
    noticeMessage: '',
    noticeShown: false,

    // Chat data and controls
    chatname: '',
    chatOpen: false,
    chatLoaded: false,
    newMessageCount: 0,
    messageHistory: [],
    messageTotal: 0,
    messageFrom: undefined,
    messageTo: undefined,

    messageTextarea: '',
    postingMessage: false,
    scrollingMessage: false,

    // Settings
    appliedSettings: false,
    editorTheme: 'monokai',
    editorTabSize: 4,
    editorSoftTabs: true,

    // Theme list
    themelist: [],

  },

  watch: {

    editorTheme: async function(){
      if (!this.appliedSettings){ return; }
      await this.setEditorTheme(this.editorTheme);
    },

    editorTabSize: async function(){
      if (!this.appliedSettings){ return; }
      await this.setEditorTabSize(this.editorTabSize);
    },

    editorSoftTabs: async function(){
      // console.log('Editor soft tabs is now =', this.editorSoftTabs);
      if (!this.appliedSettings){ return; }
      await this.setEditorSoftTabs(this.editorSoftTabs);
    },

    // syncingPost: async function(){
    //   if (!this.syncingPost){
    //     // this.triggerLocalUpdate += 1;
    //   }
    // },

    triggerLocalUpdate: async function(){
      let change = undefined;

      for (let i=0; i < this.updateQueueHold.length; i++){
        if (this.updateQueueHold[i].step === this.globalStepUpto + 1){
          change = this.updateQueueHold[i];
          // this.updateQueueHold = await _.pullAt(this.updateQueueHold, i);
          await this.processUpdateBody(change);
          return;
        }
      }

      if (!change && this.globalStepUpto < this.globalStepCounter){
        // console.log('Triggered local update. Queue:', this.updateQueue.length);

        change = this.updateQueue.shift();

        if (change.step === this.globalStepUpto + 1){
          // this.globalStepUpto += 1;
          // this.submitStepUpTo = this.globalStepUpto;
          await this.processUpdateBody(change);

        } else if (change.step > this.globalStepUpto + 1){

          this.updateQueueHold.push(change);
          console.log('Got a change that is not the expected step. Hold change.');
          // console.log(change);

          await this.sleep(50);
          this.triggerLocalUpdate += 1;
        }

      } else {
        console.log('Content up to date. Cleaning up...');
        this.updateQueueHold = [];
        this.updateQueue = [];
        this.recentUpdates = [];
        this.submittedBody = this.editor.getValue();
        if (this.triggerUpdateRemoteCache){
          this.updateRemotePostContent();
        }
        this.syncingPost = false;

      }

    },

    // triggerRemoteUpdate: async function(){
    //   // let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

    //   if (!this.processingUpdateRemoteQueue && this.updateRemoteQueue.length > 0){
    //     // console.log('Triggered local update. Queue:', this.updateQueue.length);

    //     await this.processUpdateRemote(await this.updateRemoteQueue.shift());

    //   }
    // },

    localStepCounter: async function(){
      if (this.localStepCounter >= 100){
        await this.updateRemotePostContent();
        this.localStepCounter = 0;
      }
    },

  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.editorTitle = this.formData.title;
    this.editorMode = this.formData.mode;
    this.editorBody = this.formData.content;
    this.postID = this.formData.id;
    this.lastLocalActiveAt = _.now();
  },

  mounted: async function() {
    // Hide toast
    $('#notice').toast('dispose');

    // Add window resize event listener
    await this.windowResizer();

    await this.loadEditor();

    await this.applySavedSettings();

    // Monitor socket connection
    io.socket.on('connect', async () => {
      this.socketConnected = true;
      if (this.reconnecting){
        await this.reconnectLivePost();
        await this.forceDocumentResync();
        await this.updatedPostMode();
        await this.editor.setReadOnly(false);
        this.reconnecting = false;
      }
      this.wsActive();
      // this.syncingPost = false;
    });

    io.socket.on('disconnect', async () => {
      this.reconnecting = true;
      this.socketConnected = false;
      this.syncingPost = true;
      await this.editor.setReadOnly(true);
      this.wsActive();
    });

    window.addEventListener('beforeunload', async (event) => {
      // Cancel the event as stated by the standard.
      // event.preventDefault();

      await this.disconnect();
      // Chrome requires returnValue to be set.
      event.returnValue = 'You have been disconnected.';
    });

    await this.setSocketEvents();
    await this.loadChanges();
    await this.joinLivePostSocket();
    await this.setEditorEvents();
    await this.getMessages();
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    windowResizer: async function(){
      // Initial resizing
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);

      // Then listen to event
      window.addEventListener('resize', async () => {

        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
        // this.editor.resize();

      });
    },

    setSocketEvents: async function(){
      io.socket.on('hello-editor', async (data) => {
        this.wsActive();
        console.log('An editor',data.name,'joined the party!');
        this.editorCount += 1;
      });

      io.socket.on('updateBody', async (data) => {
        // Data is a json object. No parsing needed.
        this.wsActive();
        // console.log('Got update body event.');

        // this.recentUpdates.push(data);
        if (data.step > this.globalStepCounter){
          this.globalStepCounter = data.step;
        }

        await this.recentUpdates.push(data);
        await this.updateQueue.push(data);

        this.triggerLocalUpdate += 1;

        console.log('Queued incoming step:',data.step);

        // if (this.syncingPost){
        //   this.updateQueue.push(data);
        // } else {
        //   if (data.step >= this.globalStepUpto){
        //     await this.processUpdateBody(data);
        //   }
        // }

      });

      io.socket.on('changeActiveLine', async (data) => {
        this.editor.session.addGutterDecoration(data.newRow, 'live-edit-active-line');
        this.editor.session.removeGutterDecoration(data.oldRow, 'live-edit-active-line');
      });

      io.socket.on('updateTitle', async (data) => {
        this.wsActive();
        this.editorTitle = data.title;
        document.title = data.title;
      });

      io.socket.on('updateMode', async (data) => {
        this.wsActive();
        this.editorMode = data.mode;
        await this.changeEditorMode(data.mode);
      });

      io.socket.on('editorLeft', async (data) => {
        this.wsActive();
        this.editorCount -= 1;
        console.log(data.name+' has left.');
      });

      io.socket.on('newMessage', async (data) => {
        this.wsActive();
        await this.pushMessageToHistory({
          type: data.type,
          time: data.time,
          from: data.from,
          body: data.body,
        });
        if (!this.chatOpen){
          if (this.newMessageCount < 99){
            this.newMessageCount += 1;
          }
        }
        if (!this.scrollingMessage){ await this.scrollChatToBottom(); }
      });

      io.socket.on('savedPost', async () => {
        await this.goto('/l/'+this.guestlink);
      });

      console.log('Socket events ready.');
    },

    setEditorEvents: async function(){

      this.editor.on('change', async (e) => {

        if (this.syncingPost){ return; }

        await this.processUpdateRemote(e);

      });

    },

    changeEditorMode: async function(setMode){
      // eslint-disable-next-line no-undef
      let modelist = await ace.require('ace/ext/modelist');
      let filePath = 'post.'+setMode;
      let mode = '';
      try{
        mode = await modelist.getModeForPath(filePath).mode;
      }catch(err){
        console.log(err);
      }

      await this.editor.session.setMode(mode);
    },

    updatedPostTitle: async function(){
      if (this.syncingPost){ return; }

      let params = {
        _csrf: this._csrf,
        title: this.editorTitle,
        editorlink: this.editorlink,
      };

      await io.socket.put('/api/v1/live-post/edit-live-post-title/'+this.postID, params, async (data, jwRes) => {
        this.wsActive();
        if (jwRes.statusCode < 400){
          console.log('Updated remote title.');
          document.title = this.editorTitle;
        } else {
          console.log('Error updating remote title:',jwRes.statusCode);
        }
      });
    },

    updatedPostMode: async function(){
      await this.changeEditorMode(this.editorMode);

      if (this.syncingPost){ return; }

      let params = {
        _csrf: this._csrf,
        mode: this.editorMode,
        editorlink: this.editorlink,
      };

      await io.socket.put('/api/v1/live-post/edit-live-post-mode/'+this.postID, params, async (data, jwRes) => {
        this.wsActive();
        if (jwRes.statusCode < 400){
          // console.log('Updated remote mode.');
        } else {
          console.log('Error updating remote mode:',jwRes.statusCode);
        }
      });
    },

    loadEditor: async function(){
      // eslint-disable-next-line no-undef
      this.editor = await ace.edit('editor');
      // this.editor.session.setMode('ace/mode/text');
      await this.changeEditorMode(this.editorMode);
      await this.editor.setFontSize(14);

      await this.editor.getSession().setNewLineMode('unix');
      await this.editor.getSession().setUseWrapMode(true);
      await this.editor.resize(true);
    },

    loadChanges: async function(){

      // this.socketConnected = true;

      // Clear document
      this.editorBody = '';

      console.log('Fetching changes');
      await io.socket.get('/api/v1/live-post/get-changes/'+this.postID, async (data, jwRes) => {
        this.wsActive();
        if (jwRes.statusCode < 400){
          this.syncingPost = true;

          let changes = data.changes;
          let globalStep = data.globalStep;
          let content = data.content;
          let contentStep = data.contentStep;

          console.log('Received load changes:', data);

          // Update title, mode, editor count
          this.editorTitle = data.title;
          this.editorMode = data.mode;
          this.submittedBody = data.content;

          // Update counters
          this.globalStepCounter = globalStep;
          this.globalStepUpto = contentStep;
          this.submitStepUpTo = contentStep;

          // If there is no global step or cache content step, nothing is typed yet.
          if (!globalStep && !contentStep){
            this.syncingPost = false;
            return;
          }

          console.log('Total steps:', globalStep);
          console.log('Cache content step:', contentStep);

          // Apply cache contents
          await this.editor.setValue(content, -1);

          // Apply changes if there is any
          if (changes){
            console.log('Expected changes after cache:', globalStep - contentStep);
            console.log('Received changes after cache:', changes.length);
            if (changes.length > 100) {
              this.triggerUpdateRemoteCache = true;
            }
            for (let i=0; i < changes.length; i++){
              // await this.processUpdateBody(JSON.parse(changes[i]));
              let change = JSON.parse(changes[i]);

              if (change.step > this.globalStepUpto){
                // this.recentUpdates.push(JSON.parse(changes[i]));
                await this.updateQueue.push(JSON.parse(changes[i]));
              }
            }
          }

          // this.syncingPost = false;
          this.triggerLocalUpdate += 1;

          return;

        } else {
          console.log('Error fetching changes:',jwRes.statusCode);
          this.syncingPost = false;
        }
      });

    },

    forceDocumentResync: async function(){
      // Force a resync
      console.log('Forcing document resync...');
      this.syncingPost = true;
      $('#notice').toast('dispose');

      // Stop all local modifications
      await this.editor.setReadOnly(true);

      try{
        // await io.socket.reconnect();
      }catch(err){
        console.log('Socket is already connected:', err.message);
      }

      // Reset local contents
      this.updateQueue = [];
      this.recentUpdates = [];
      this.historyQueue = [];
      this.localStepCounter = 0;
      this.cloudError = '';

      await this.loadChanges();
      // await this.updateRemotePostContent();
      await this.updatedPostMode();
      // this.syncingPost = false;
      await this.editor.setReadOnly(false);
    },

    joinLivePostSocket: async function(){
      let params = {
        _csrf: this._csrf,
        editorlink: this.editorlink,
      };

      await io.socket.post('/api/v1/live-post/hello-live-post/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400){
          console.log(data);
          console.log('Your name is:', this.chatname);
          // console.log('You are with',data.editorCount-1,'others.');
          // this.chatname = data.name;
          // this.editorCount = data.editorCount;
        } else {
          console.log('Error saying hello:',jwRes.statusCode);
        }
      });

    },

    reconnectLivePost: async function(){
      let params = {
        _csrf: this._csrf,
        editorlink: this.editorlink,
      };

      await io.socket.post('/api/v1/live-post/reconnect-live-post/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400){
          console.log(data);
        } else {
          console.log('Error reconnecting:',jwRes.statusCode);
        }
      });

    },

    processUpdateRemote: async function(e){
      // console.log('Processing update remote. SyncingPost:', this.syncingPost);
      if (this.syncingPost){ return; }

      let ignore = false;
      let expectedStep = this.submitStepUpTo + 1;
      this.submitStepUpTo += 1;

      // Search for matching recent changes
      let length = this.recentUpdates.length;
      console.log('Total recent changes:', length);
      // console.log(this.recentUpdates);

      for (let i=0; i < length; i++){
        let update = {};
        if (this.recentUpdates[i]){
          update = this.recentUpdates[i].change;
        } else {
          continue;
        }

        if (update.action === e.action && _.difference(update.lines, e.lines).length === 0 ){
          // If the lines are the same, check starting position
          if (update.start.row === e.start.row && update.start.column === e.start.column){
            // console.log('Change is matched as a recent incoming update. Not submitting the change.');
            // Remove the recent changes entry
            // while (this.processingRecentUpdate){
            //   await this.sleep(this.sleepDelay);
            // }
            // this.processingRecentUpdate = true;
            // this.recentUpdates = await _.pullAt(this.recentUpdates, i);
            // this.processingRecentUpdate = false;

            ignore = true;
            break;
          }

        }

      }

      if (!ignore){
        // console.log('The change is not in recent changes. Sumitting change.');
        await this.updateRemotePost(e, expectedStep);
      }

    },

    updateRemotePost: async function(e, step){
      let submitBody = await this.editor.getValue();

      // if (!this.socket.isConnected()){
      //   try{
      //     await this.socket.reconnect();
      //   }catch(err){
      //     console.log('Socket is already connected:', err.message);
      //   }
      // }

      let change = e;
      change.step = step;

      // this.processingUpdateRemoteQueue = true;
      // console.log('Updating remote post...');
      let params = {
        change: change,
        step: step,
        editorlink: this.editorlink,
        _csrf: this._csrf
      };

      await io.socket.put('/api/v1/live-post/edit-live-post/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400 ){

          // let change = {
          //   step: data.globalStep,
          //   change: e,
          // };

          // console.log('Submitted step confirm:',change.step);
          // console.log('Last incoming change:', this.historyQueue[this.historyQueue.length - 1]);

          // Update counters
          this.localStepCounter += 1;

          if (data.globalStep > data.submitStepUpTo){
            this.submitStepUpTo = data.globalStep;
          }
          if (data.globalStep > this.globalStepUpto){
            this.globalStepUpto = data.globalStep;
          }

          console.log('Submit:', change.lines, '. Submit step upto:', this.submitStepUpTo);
          console.log('Submitted step:', data.globalStep);

          // Update submitted body
          this.submittedBody = submitBody;


          return;
        } else {
          console.log('Error updating remote post:', jwRes.statusCode);
          console.log(jwRes);
          await this.editor.setReadOnly(true);
          this.cloudError = 'Server Error';
          this.notify('Oops!', 'Data is out of sync on the server. Some changes could have been lost. Please reload the page to resume editing.');
        }
      });

      // this.processingUpdateRemoteQueue = false;
      // this.triggerRemoteUpdate += 1;

    },

    processUpdateBody: async function(entry){
      if (!entry){ return; }
      if (this.cloudError){ return; }
      // console.log('Processing step:', entry.step);
      // this.globalStepUpto += 1;

      // Check step
      // If entry step is not the step after the globalStepUpto, check updateQueue for the step
      if (entry.step !== this.globalStepUpto + 1){
        console.log('WARNING! Processing step is not the next step expected.');
        console.log('Processing step:', entry.step,'. Expected:', this.globalStepUpto);
        this.notify('Warning', 'Some contents may be out of sync. Please try the resync button.');
      }

      // Apply change and update step count
      // this.globalStepUpto = entry.step;
      let e = entry.change;

      let rev = this.editor.session.$undoManager.startNewGroup(); // start new undo group

      if (e.action === 'insert'){

        let lines = '';
        for (let i=0; i < e.lines.length; i++){
          lines += e.lines[i];
          if (i !== e.lines.length - 1){
            lines += '\n';
          }
        }

        this.editor.session.insert(e.start, lines);

      } else if (e.action === 'remove'){
        let range = {
          start: e.start,
          end: e.end,
        };

        this.editor.session.remove(range);
      }

      if (entry.step > this.globalStepUpto){
        this.globalStepUpto = entry.step;
      }

      if (entry.step > this.submitStepUpTo){
        this.submitStepUpTo = entry.step;
      }

      // console.log('Process body:', e.lines,'. Submit step upto:', this.submitStepUpTo);

      this.editor.session.$undoManager.markIgnored(rev);

      // this.historyQueue.push(entry);

      // Trigger local update after body is updated.
      // await this.sleep(0.5);
      this.triggerLocalUpdate += 1;
      return;

    },

    updateRemotePostContent: async function(){

      // console.log('Updating remote post content...');
      let params = {
        content: this.submittedBody,
        step: this.globalStepUpto,
        editorlink: this.editorlink,
        _csrf: this._csrf
      };

      // console.log('Updating remote post cache...');
      // console.log('Current global step up to:', this.globalStepUpto);

      await io.socket.post('/api/v1/live-post/update-live-post-content/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400 ){
          // console.log('Updated remote post cache.');
          return;
        } else {
          console.log('Error updating remote post:', jwRes.statusCode);
        }
      });

    },

    notify: async function(title, message){
      this.noticeTitle = title;
      this.noticeMessage = message;
      this.noticeShown = true;

      await $('#notice').toast('show');
    },

    // updateEditorCount: async function(){
    //   await io.socket.get('/api/v1/live-post/get-room-editor-count/'+this.postID, (data, jwRes) => {
    //     if (jwRes.statusCode < 400 ){
    //       console.log('Updated editor count:', data);
    //       this.editorCount = data.editorCount;
    //       return;
    //     } else {
    //       console.log('Error getting editor count:', jwRes.statusCode);
    //     }
    //   });
    // },


    disconnect: async function(){
      let params = {
        editorlink: this.editorlink,
        name: this.chatname,
        _csrf: this._csrf
      };

      this.unloaded = true;
      this.editor.setReadOnly(true);

      await io.socket.post('/api/v1/live-post/disconnect-editor/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400 ){
          console.log('Disconnected');
          return;
        } else {
          console.log('Error disconnecting:', jwRes.statusCode);
        }
      });

      return;
    },

    saveLivePost: async function(){
      let params = {
        editorlink: this.editorlink,
        _csrf: this._csrf,
      };
      await io.socket.post('/api/v1/live-post/save-live-post/'+this.postID, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400 ){
          console.log('Saved post.');
          await this.goto('/l/'+this.guestlink);
          return;
        } else {
          console.log('Error saving post:', jwRes.statusCode);
        }
      });
    },

    copyLinkToClipboard: async function(link){
      const el = document.createElement('textarea');
      el.value = this.baseUrl + link;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      // this.copiedResponse = 'Copied!';
    },

    // Modals
    openQrCode: async function(type){
      if (type === 'editor'){
        this.qrCodeToShow = this.editorQrcode;
        this.qrCodeUrl = this.baseUrl+'/le/'+this.editorlink;
        this.modal = 'qrcode';
      } else if (type ==='guest'){
        this.qrCodeToShow = this.guestQrcode;
        this.qrCodeUrl = this.baseUrl+'/l/'+this.guestlink;
        this.modal = 'qrcode';
      }
    },

    closeQrCode: async function(){
      this.modal = '';
    },

    openSettingsModal: async function(){
      this.modal = 'settings';
    },

    closeSettingsModal: async function(){
      this.modal = '';
    },

    // Settings
    applySavedSettings: async function(){
      //eslint-disable-next-line no-undef
      let themelist = await ace.require('ace/ext/themelist');
      // let themes = await themelist.themesByName;
      this.themelist = await themelist.themesByName;
      // console.log(this.themelist);

      let theme = await this.getCookie('editor_theme');
      // console.log('Got theme:', theme);
      if (theme){
        this.editorTheme = theme;
      }
      await this.setEditorTheme(this.editorTheme);

      // let tabSize = await this.getCookie('editor_tab_size');
      // // console.log('Got theme:', theme);
      // if (tabSize !== undefined){
      //   this.editorTabSize = tabSize;
      // }
      await this.setEditorTabSize(this.editorTabSize);

      // let softTabs = await this.getCookie('editor_soft_tabs');
      // if (softTabs !== undefined){
      //   this.editorSoftTabs = softTabs;
      // }
      await this.setEditorSoftTabs(this.editorSoftTabs);

      this.appliedSettings = true;
    },

    setEditorTheme: async function(theme){
      console.log('Setting theme:', theme);
      if (!theme){ return; }
      await this.editor.setTheme('ace/theme/'+theme);
      // await this.setCookie('editor_theme', theme);
    },

    setEditorTabSize: async function(size){
      console.log('Setting tab size:',size);
      if (!size){ return; }
      await this.editor.session.setTabSize(size);
      // await this.setCookie('editor_tab_size', size);
    },

    setEditorSoftTabs: async function(arg){
      console.log('Setting soft tabs:',arg);
      await this.editor.session.setUseSoftTabs(arg);
      // await this.setCookie('editor_soft_tabs', arg);
    },

    // Misc
    setCookie: async function(name, data){
      document.cookie = `${name}=${data}; max-age=315360000; path=/; SameSite=Lax`;
    },

    getCookie: async function(name){
      const cookies = document.cookie.split('; ');

      let value = undefined;
      for (let i=0; i < cookies.length; i++){
        if (cookies[i] === ''){ continue; }
        let item = cookies[i].split('=')[0];
        if (item === name){
          value = cookies[i].split('=')[1];
          break;
        }
      }

      return value;
    },

    wsActive: async function(){
      this.wsActivity = true;
      await this.sleep(50);
      this.wsActivity = false;
    },

    sleep: function(ms){
      return new Promise(resolve => setTimeout(resolve, ms));
    },

    // Chat functions
    toggleChat: async function(){
      this.chatOpen = !this.chatOpen;
      await this.sleep(100);
      this.newMessageCount = 0;
      await this.editor.resize();
    },

    pushMessageToHistory: async function(message){
      await this.messageHistory.push(message);
    },

    prependMessageToHistory: async function(message){
      await this.messageHistory.unshift(message);
    },

    sendMessage: async function(){
      if (!this.messageTextarea){ return; }
      this.postingMessage = true;

      let text = await this.messageTextarea.trim();

      // Check message size
      if (text.length > 2000){
        this.notify('Hol\' up a minute...', 'You are sending more than 2000 characters! Current at: '+text.length+'. Please reduce your message size.');
        this.postingMessage = false;
        return;
      }

      if (!await io.socket.isConnected()){
        await this.forceReloadMessages();
        this.notify('Reloaded', 'Document is reloaded due to inactivity.');
        return;
      }

      let message = {
        type: 'message',
        from: this.chatname,
        time: _.now(),
        body: text,
        chatKey: this.formData.chatKey,
        _csrf: this._csrf,
      };

      let url = '/api/v1/chat/post-message/'+ this.postID;

      // Post message
      await io.socket.post(url, message, async (data, jwRes) => {
        if (jwRes.statusCode < 400){
          // Push message to history and clear textarea
          this.messageTextarea = '';
          await this.pushMessageToHistory({
            type: 'message',
            from: message.from,
            time: message.time,
            body: message.body,
          });
          await this.scrollChatToBottom();
          $( '#chat-textarea' ).focus();

        } else {
          console.log('Error sending message:', jwRes.body);
        }

        this.postingMessage = false;
      });
    },

    getMessages: async function(append = true, from, to){
      if (from > to){
        console.log('Invalid arguments.');
        return; }

      let params = {
        from: from,
        to: to,
      };

      if (append && from && to){
        if (params.from <= this.messageTo && this.messageTo < this.messageTotal - 1){
          params.from = this.messageTo + 1;
        }
        if (this.messageTo < this.messageTotal - 1){
          params.to = this.messageTo + 1;
        }
      }

      // if (params.from >= this.messageFrom && this.messageFrom > 0){ params.from = this.messageFrom - 1; }
      // if (params.to <= this.messageTo && this.messageTo < this.messageTotal - 1){ params.to = this.messageTo + 1; }

      // console.log('Fetching messages from: ' + from + ' to: ', to);

      let url = '/api/v1/chat/get-messages/'+ this.postID;

      await io.socket.get(url, params, async (data, jwRes) => {
        if (jwRes.statusCode < 400){
          this.messageTotal = data.total;

          if (this.messageFrom > data.from || this.messageFrom === undefined){
            this.messageFrom = data.from;
          }

          if (this.messageTo < data.to || this.messageTo === undefined){
            this.messageTo = data.to;
          }

          // console.log('Total messages:', this.messageTotal);
          // console.log('Loaded messages from ID:', this.messageFrom,'to',this.messageTo);

          let messages = data.messages;

          if (!messages.length){
            messages = [messages];
          }

          this.messageTextarea = '';
          if (append){
            for (let i=0; i < messages.length; i++){
              let message = JSON.parse(messages[i]);
              await this.pushMessageToHistory({
                type: message.type,
                from: message.from,
                time: message.time,
                body: message.body,
              });
            }
            this.scrollChatToBottom();
          } else {
            for (let i=messages.length-1; i>=0; i--){
              let message = JSON.parse(messages[i]);
              await this.prependMessageToHistory({
                type: message.type,
                from: message.from,
                time: message.time,
                body: message.body,
              });
            }
          }

          this.chatLoaded = true;

        } else {
          console.log('Error fetching messages:', jwRes.body);
        }
      });
    },

    scrollChatToBottom: async function(){
      let div = document.getElementById('chat-history');
      // div.scrollTop = div.scrollHeight - div.clientHeight;
      await $('#chat-history').animate({
        scrollTop: div.scrollHeight - div.clientHeight
      }, 200);
      // $('#chat-history').scrollTop(div.scrollHeight - div.clientHeight);
    },

    processHistoryScrolling: async function(){
      // console.log($('#chat-history').scrollTop() - $('#chat-history').prop('scrollHeight') + $('#chat-history').prop('clientHeight'));

      if ($('#chat-history').scrollTop() - $('#chat-history').prop('scrollHeight') + $('#chat-history').prop('clientHeight') < -20) {
        this.scrollingMessage = true;
      } else {
        this.scrollingMessage = false;
      }
    },

    loadEarlierMessages: async function() {
      if (this.messageFrom <= 0){
        return;
      }

      let to = this.messageFrom - 1;
      let from = this.messageFrom - 25;

      if (from < 0){
        from = 0;
      }

      // console.log('Pressed button to load: '+from+' to '+to);

      await this.getMessages(false, from, to);

    },

    forceReloadMessages: async function(){
      this.chatLoaded = false;
      this.ewMessageCount = 0;
      this.messageHistory = [];
      this.messageTotal = 0;
      this.messageFrom = undefined;
      this.messageTo = undefined;

      await this.getMessages();
    }


  }
});
