parasails.registerPage('read-post', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    formData: {},
    modal: '',

    editor: undefined,
    noticeTitle: '',
    noticeMessage: '',
    noticeShown: false,
    copiedResponse: '',

    baseUrl: '',
    qrcode: '',

    mode: '',
  },

  watch: {
    copiedResponse: async function(){
      if (this.copiedResponse !== ''){
        let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
        await sleep(1000);
        this.copiedResponse = '';
      }
    },

    mode: function(){
      if (this.mode !== ''){
        // eslint-disable-next-line no-undef
        let modelist = ace.require('ace/ext/modelist');
        let filePath = 'post.'+this.mode;
        let mode = '';
        try{
          mode = modelist.getModeForPath(filePath).mode;
        }catch(err){
          console.log(err);
        }

        this.editor.session.setMode(mode);
      }
    },
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    this.loadEditor();
    this.mode = this.formData.mode;
    if (this.mode === ''){
      this.mode = 'txt';
    }
    $('#notice').toast('hide');
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    loadEditor: function(){
      // eslint-disable-next-line no-undef
      this.editor = ace.edit('editor');
      this.editor.setTheme('ace/theme/monokai');
      // this.editor.session.setMode('ace/mode/text');

      this.editor.setFontSize(14);
      this.editor.getSession().setNewLineMode('unix');
      this.editor.getSession().setUseWrapMode(true);
      this.editor.setReadOnly(true);
      this.editor.setValue(this.formData.content, -1);
      this.editor.resize(true);
    },

    notify: function(title, message){
      this.noticeTitle = title;
      this.noticeMessage = message;
      this.noticeShown = true;

      $('#notice').toast('show');
    },

    copyLinkToClipboard: function(){
      const el = document.createElement('textarea');
      el.value = this.baseUrl + '/p/' + this.formData.shortlink;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      this.copiedResponse = 'Copied!';
    },

    openQrCode: function(){
      this.modal = 'qrcode';
    },

    closeQrCode: function(){
      this.modal = '';
    },

  }
});
